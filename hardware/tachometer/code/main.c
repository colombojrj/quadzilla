/*************************************************************************
Project:    Digital tachometer with photo transistors
Author:     José Roberto Colombo Junior  <colombojrj[at]gmail.com>
Compiler:   AVR-GCC 4.8.2, Kubuntu Linux 14.04
Created:    14/02/2016
**************************************************************************/

#include "drivers.h"
#include "timer.h"
#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// Global variables
uint16_t one_second = 0;
uint8_t num_coded[3], update_display = 0;
volatile uint16_t M1_count = 0, M2_count = 0, M3_count = 0, M4_count = 0;
uint8_t changedbits;
volatile uint8_t portahistory = 0xFF;

// Pin interrupts
ISR (PCINT0_vect) {
	changedbits = PINA ^ portahistory;
	portahistory = PINA;

	/* PCINT0 changed */
	if(changedbits & (1 << PA0)) {
		M1_count++;
	}
}

// 1ms interrupt
ISR (TIM0_COMPA_vect) {
	// Increment the counter
	one_second++;
	update_display++;

	// Update the hundreds
	if (update_display == 10) {
		UPDATE_DIGIT(num_coded[0], ((1 << QD) | (1 << QE)));
	}

	// Update the tens
	if (update_display == 20) {
		UPDATE_DIGIT(num_coded[1], ((1 << QC) | (1 << QE)));
	}

	// Update the units
	if (update_display == 30) {
		UPDATE_DIGIT(num_coded[2], ((1 << QC) | (1 << QD)));
		update_display = 0;
	}

	// Update the number to be showed in the display
	if (one_second == 1000) {
		one_second = 0;

		// Update the digit
		SEPARATE_NUMBER(M1_count, num_coded);

		// Reset the counts for all motors
		M1_count = 0;
	}
}


int main() {
	DDRA = 0;
	PORTA= 0;
	DDRB = 0;
	PORTB= 0;

	// Initialize the 74LS595
	SETUP_SHIFT();

	// Initialize TIMER0 to overflow in 1ms
	INIT_TIMER0 (0x7C);

	// Enable interrupts for the pins PA0 (PCINT0), PA1 (PCINT1), PA2 (PCINT2) and PA3 (PCINT3)
	PCMSK0 = (1 << PCINT0) | (0 << PCINT1) | (0 << PCINT2) | (0 << PCINT3);
	GIMSK = (1 << PCIE0);
	sei();

	// Power reduction
	PRR = (1 << PRUSI) | (1 << PRADC);

	// Forever loop
	while(1){}



	return 0;
}
