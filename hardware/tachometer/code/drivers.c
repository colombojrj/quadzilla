/*
 * drivers.c
 *
 *  Created on: 14/02/2016
 *      Author: junior
 */

#include "drivers.h"

////////////////////////////////
// Shift-out: 74LS595 library //
////////////////////////////////
void SETUP_SHIFT() {
	SHIFT_STCP_DDR |= SHIFT_STCP_PIN;
	SHIFT_SHCP_DDR |= SHIFT_SHCP_PIN;
	SHIFT_DS_DDR   |= SHIFT_DS_PIN;
}

void LATCH_UP() {
	SHIFT_STCP_PORT |= SHIFT_STCP_PIN;
}

void LATCH_DOWN() {
	SHIFT_STCP_PORT &=~SHIFT_STCP_PIN;
}

void SHIFT_WRITE(uint8_t val) {
	uint8_t i, data;

	LATCH_DOWN();
	for (i = 0; i < 8; i++)  {

		// Send data
		data = val & (1 << i);
		if (data) {
			SHIFT_DS_PORT |= SHIFT_DS_PIN;
		} else {
			SHIFT_DS_PORT &=~SHIFT_DS_PIN;
		}

		// One pulse on clockPin
		SHIFT_SHCP_PORT |= SHIFT_SHCP_PIN;
		SHIFT_SHCP_PORT &=~SHIFT_SHCP_PIN;
	}
	LATCH_UP();
}

uint8_t CODE_NUMBER(uint8_t n) {
	uint8_t number = 0;

	if (n & 0b0001) {
		number |= (1 << QF);
	}
	if (n & 0b0010) {
		number |= (1 << QA);
	}
	if (n & 0b0100) {
		number |= (1 << QH);
	}
	if (n & 0b1000) {
		number |= (1 << QG);
	}

	return number;
}

/*
 * Function to extract
 * Inputs:
 *   n: uint16_t number (but it is smaller than 999)
 * Returns:
 *   None
 */
void SEPARATE_NUMBER(uint16_t number, uint8_t *array) {
	// Initialize the array with zeros
	array[0] = 0;
	array[1] = 0;

	// Extract the hundreds
	while (number >= 100) {
		number = number -100;
		array[0] = array[0] +1;
	}

	// Extract the tens
	while (number >= 10) {
		number = number -10;
		array[1] = array[1] +1;
	}

	// Extract the units
	array[2] = number;
}

/*
 * Function to update the display
 * Inputs:
 *   n: uint16_t number (but it is smaller than 999)
 * Returns:
 *   None
 */
void UPDATE_DIGIT(uint8_t number, uint8_t digit) {
	SHIFT_WRITE(CODE_NUMBER(number) | digit);
}


