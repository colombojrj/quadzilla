/*
 * drivers.h
 *
 *  Created on: 14/02/2016
 *      Author: junior
 */

#ifndef DRIVERS_H_
#define DRIVERS_H_

#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

////////////////////////////////
// Shift-out: 74LS595 library //
////////////////////////////////

// DATA INSIDE the 74LS595
//    QA QB QC QD QE QF QG QH
// 0b 1  1  1  1  1  1  1  1  (uint8_t byte)

#define QA 7
#define QB 6
#define QC 5
#define QD 4
#define QE 3
#define QF 2
#define QG 1
#define QH 0

#define DIG_1 QE
#define DIG_2 QD
#define DIG_3 QC

#define SHIFT_STCP_PORT PORTB
#define SHIFT_STCP_DDR  DDRB
#define SHIFT_STCP_PIN  (1 << PB1)
#define SHIFT_SHCP_PORT PORTB
#define SHIFT_SHCP_DDR  DDRB
#define SHIFT_SHCP_PIN  (1 << PB0)
#define SHIFT_DS_PORT   PORTB
#define SHIFT_DS_DDR    DDRB
#define SHIFT_DS_PIN    (1 << PB2)

extern void SETUP_SHIFT();
extern void LATCH_UP();
extern void LATCH_DOWN();
extern void SHIFT_WRITE(uint8_t val);
uint8_t shift_word;



extern uint8_t CODE_NUMBER(uint8_t n);
extern void SEPARATE_NUMBER(uint16_t number, uint8_t *array);
extern void UPDATE_DIGIT(uint8_t number, uint8_t digit);



#endif /* DRIVERS_H_ */
