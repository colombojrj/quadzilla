/*
  timer.c - Light library to manage the TIMERS of ATMEGA microcontrollers

  Copyright (c) 2015 - José Roberto Colombo Junior (colombojrj@gmail.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA
*/

#include "timer.h"

//
// TODO: add these other operation modes to the library
//

/*****************
 * TIMER0 MODULE
 */

/*
void timer0_init() {
    // set up timer with external clock source
    TCCR0 |= (1 << CS02) | (1 << CS01) | (0 << CS00);
    // initialize counter
    TCNT0 = 0;
    // enable overflow interrupt
    TIMSK |= (1 << TOIE0);
}

void INIT_TIMER1 () {
    OCR1A = 0xF423; // 100ms => 0x1869 and 1000ms => 0xF423
    TCCR1B |= (1 << WGM12);
    // Mode 4, CTC on OCR1A
    TIMSK |= (1 << OCIE1A);
    //Set interrupt on compare match
    TCCR1B |= (1 << CS12);
    // set prescaler to 1024 and start the timer
    sei();
}

 */

#if   TIMER0_CONFIG == NORMAL
	#if defined (__AVR_ATmega8__)
		void INIT_TIMER0 () {
			TIMSK = (1 << TOIE0); // Enable overflow interrupt
			TCCR0 = TIMER0_CLOCK;
			TCNT0 = 0;
			sei();
		}
	#endif
#elif TIMER0_CONFIG == CTC
	#if defined (__AVR_ATmega328P__)
		void INIT_TIMER0 (uint8_t TOP_OCR0A) {
			TIMSK0 = (1 << OCIE0A); // Enable output compare interrupt
			TCCR0A = (1 << WGM01);
			TCCR0B = TIMER0_CLOCK;
			TCNT0 = 0;
			OCR0A = TOP_OCR0A;
			sei();
		}
	#endif
	#if defined (__AVR_ATtiny24__) || defined (__AVR_ATtiny24A__)
		void INIT_TIMER0 (uint8_t TOP_OCR0A) {
			TIMSK0 = (1 << OCIE0A) | (1 << TOIE0); // Enable output compare interrupt
			TCCR0A = (1 <<  WGM01);
			TCCR0B = TIMER0_CLOCK;
			TCNT0 = 0;
			OCR0A = TOP_OCR0A;
			sei();
		}
	#endif
#elif TIMER0_CONFIG == PWM_OCR0A
#elif TIMER0_CONFIG == PWM_OCR0B
#elif TIMER0_CONFIG == PWM_OCR0AB
	/*
	 * Function: INIT_TIMER0 (uint8_t duty_A, uint8_t duty_B)
	 * Purpose:  start TIMER0 as FAST PWM
	 * Receives: duty_A and duty_B are the duty to OC0A and OC0B, respectively
	 * Returns:  nothing
	 */
	void INIT_TIMER0 (uint8_t duty_A, uint8_t duty_B) {
		TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);
		TCCR0B = TIMER0_CLOCK;
		TCNT0 = 0;
		OCR0A = duty_A;
		OCR0B = duty_B;
	}
#elif TIMER0_CONFIG == PHASE_CORRECT
#endif
/*
 * Function: TIMER0_OFF ()
 * Purpose:  stop TIMER0 and deactivate the interrupts
 * Receives: nothing
 * Returns:  nothing
 */
#if TIMER0_CONFIG != OFF
	#if defined (__AVR_ATmega8__)
	void TIMER0_OFF() {
			TCCR0  = 0;
			TCNT0  = 0;
			TIMSK &= ~(1 << TOIE0);
			TIFR  &= ~(1 << TOV0);
		}
	#endif
	#if defined (__AVR_ATmega328P__)
		void TIMER0_OFF() {
			TCCR0A = 0;
			TCCR0B = 0;
			TCNT0 = 0;
			OCR0A = 0;
			OCR0B = 0;
			TIMSK0 = 0;
			TIFR0 = 0;
		}
	#endif
#endif

/*
 * TIMER1 MODULE
 */

/*
OCR1A = 0xF423; // 100ms => 0x1869 and 1000ms => 0xF423
TCCR1B |= (1 << WGM12);
// Mode 4, CTC on OCR1A
TIMSK |= (1 << OCIE1A);
//Set interrupt on compare match
TCCR1B |= (1 << CS12);
// set prescaler to 1024 and start the timer
sei();
 */
#if TIMER1_CONFIG == NORMAL
#elif TIMER1_CONFIG == CTC
	#if defined (__AVR_ATmega8__)
		void INIT_TIMER1 (uint16_t TOP_OCR1A) {
			OCR1A   = TOP_OCR1A;
			TCCR1B |= (1 << WGM12) | TIMER1_CLOCK;
			TIMSK  |= (1 << OCIE1A); // Enable output compare interrupt
			TCCR1A  = 0;
			TCNT1   = 0;
			sei();
		}
	#endif
	#if defined (__AVR_ATmega328P__)
		void INIT_TIMER1 (uint16_t TOP_OCR1A) {
			TIMSK1 = ((1 << OCIE1A)); // Enable output compare interrupt
			TCCR1A = 0;
			TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
			TCNT1 = 0;
			_TOP_OCR1A = TOP_OCR1A;
			OCR1A = _TOP_OCR1A;
			sei();
		}
	#endif
#elif TIMER1_CONFIG == PWM_OCR1A
	void TIMER1_SET_DUTY (uint16_t DUTY) {
		#if RESOLUTION == 8
			OCR1A = (uint8_t) DUTY & 0xFF;
		#elif RESOLUTION == 9
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b00000001) >> 8;
		#elif RESOLUTION == 10
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b00000011) >> 8;
		#elif RESOLUTION == 11
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b00000111) >> 8;
		#elif RESOLUTION == 12
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b00001111) >> 8;
		#elif RESOLUTION == 13
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b00011111) >> 8;
		#elif RESOLUTION == 14
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b00111111) >> 8;
		#elif RESOLUTION == 15
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b01111111) >> 8;
		#elif RESOLUTION == 16
			OCR1AL = DUTY & 0xFF;
			OCR1AH = (DUTY & 0b11111111) >> 8;
		#endif
	}
	void INIT_TIMER1 (uint16_t DUTY) {
		#if RESOLUTION == 8
			TCCR1A = (1 << COM1A1) | (1 << COM1A0) | (1 << WGM10);
			TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
		#elif RESOLUTION == 9
			TCCR1A = (1 << COM1A1) | (1 << WGM11);
			TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
		#elif RESOLUTION == 10
			TCCR1A = (1 << COM1A1) | (1 << WGM10) | (1 << WGM11);
			TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
		#elif RESOLUTION == 16
			TCCR1A = (1 << COM1A1) | (1 << WGM11);
			TCCR1B = (1 << WGM13) | (1 << WGM12) | TIMER1_CLOCK;
		#endif
		TCNT1 = 0;
		TIMER1_SET_DUTY (DUTY);
		//OCR1A = DUTY;
	}

#elif TIMER1_CONFIG == PWM_OCR1B
	void TIMER1_SET_DUTY (uint16_t DUTY) {
			#if RESOLUTION == 8
				OCR1B = (uint8_t) DUTY & 0xFF;
			#elif RESOLUTION == 9
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b00000001) >> 8;
			#elif RESOLUTION == 10
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b00000011) >> 8;
			#elif RESOLUTION == 11
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b00000111) >> 8;
			#elif RESOLUTION == 12
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b00001111) >> 8;
			#elif RESOLUTION == 13
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b00011111) >> 8;
			#elif RESOLUTION == 14
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b00111111) >> 8;
			#elif RESOLUTION == 15
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b01111111) >> 8;
			#elif RESOLUTION == 16
				OCR1BL = DUTY & 0xFF;
				OCR1BH = (DUTY & 0b11111111) >> 8;
			#endif
		}
		void INIT_TIMER1 (uint16_t DUTY) {
			#if RESOLUTION == 8
				TCCR1A = (1 << COM1B1) | (1 << WGM10);
				TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
			#elif RESOLUTION == 9
				TCCR1A = (1 << COM1B1) | (1 << WGM11);
				TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
			#elif RESOLUTION == 10
				TCCR1A = (1 << COM1B1) | (1 << WGM10) | (1 << WGM11);
				TCCR1B = (1 << WGM12) | TIMER1_CLOCK;
			#elif RESOLUTION == 16
				TCCR1A = (1 << COM1B1) | (1 << WGM11);
				TCCR1B = (1 << WGM13) | (1 << WGM12) | TIMER1_CLOCK;
			#endif
			TCNT1 = 0;
			TIMER1_SET_DUTY (DUTY);
		}

#elif TIMER1_CONFIG == PWM_OCR1AB
	void TIMER1_SET_DUTY (uint16_t DUTY_A, uint16_t DUTY_B) {
		// Do something!
	}
	void INIT_TIMER1 (uint8_t duty_A, uint8_t duty_B) {
		TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);
		TCCR0B = TIMER0_CLOCK;
		TCNT0 = 0;
		OCR0A = duty_A;
		OCR0B = duty_B;
	}
#elif TIMER1_CONFIG == PHASE_CORRECT
#endif

#if TIMER1_CONFIG != OFF
#if defined (__AVR_ATmega8__)
	void TIMER1_OFF() {
		TCCR1A = 0;
		TCCR1B = 0;
		TCNT1 = 0;
		OCR1A = 0;
		OCR1B = 0;
		ICR1 = 0;
	}
	#endif
	#if defined (__AVR_ATmega328P__)
	void TIMER1_OFF() {
		TCCR1A = 0;
		TCCR1B = 0;
		TCNT1 = 0;
		OCR1A = 0;
		OCR1B = 0;
		TIMSK1 = 0;
		TIFR1 = 0;
		ICR1 = 0;
	}
	#endif

#endif
