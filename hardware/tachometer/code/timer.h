/*
  timer.h - Light library to manage the TIMERS of ATMEGA microcontrollers

  Copyright (c) 2015 - José Roberto Colombo Junior (colombojrj@gmail.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA
*/

#ifndef TIMER_H_
#define TIMER_H_

#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>

//////////////////////////////////
// Operation mode configuration //
//////////////////////////////////
#define TIMER0_CONFIG CTC
#define TIMER0_CLOCK  CLK_8
#define TIMER1_CONFIG OFF
#define TIMER1_CLOCK  CLK_256
#define RESOLUTION    8          		// in bits (support for 8,9,10)

// Useful defines
#define OFF              0 // TIMER without configuration
#define NORMAL           1
#define CTC              2
#define PWM_OCR0A        3 // only OCR0A works as fast-PWM
#define PWM_OCR0B        4 // only OCR0B works as fast-PWM
#define PWM_OCR0AB       5 // both OCR0A and OCR0B works as fast-PWM
#define PHASE_CORRECT    6
#define PWM_OCR1A        7 // only OCR1A works as fast-PWM
#define PWM_OCR1B        8 // only OCR1B works as fast-PWM
#define PWM_OCR1AB       9 // both OCR1A and OCR1B works as fast-PWM

// ATtiny24A, ATtiny24, ATmega328p, ATmega8
#define NO_CLOCK      0
#define NO_PREESCALE  1
#define CLK_8         2
#define CLK_64        3
#define CLK_256       4
#define CLK_1024      5
#define T0_FALLING    6
#define T0_RAISING    7

// Functions configuration
#if   TIMER0_CONFIG == NORMAL
	extern void INIT_TIMER0 ();
#elif TIMER0_CONFIG == CTC
	extern void INIT_TIMER0 (uint8_t TOP_OCR0A);
#elif TIMER0_CONFIG == PWM_OCR0A
#elif TIMER0_CONFIG == PWM_OCR0B
#elif TIMER0_CONFIG == PWM_OCR0AB
	extern void INIT_TIMER0 (uint8_t duty_A, uint8_t duty_B);
#endif
#if TIMER0_CONFIG != OFF
	extern void TIMER0_OFF();
#endif

#if   TIMER1_CONFIG == NORMAL
	extern void INIT_TIMER1 ();
#elif TIMER1_CONFIG == CTC
	extern void INIT_TIMER1 (uint16_t TOP_OCR1A);
#elif TIMER1_CONFIG == PWM_OCR1A
	extern void INIT_TIMER1 (uint16_t duty);
	extern void TIMER1_SET_DUTY (uint16_t DUTY);
#elif TIMER1_CONFIG == PWM_OCR1B
	extern void INIT_TIMER1 (uint16_t duty);
	extern void TIMER1_SET_DUTY (uint16_t DUTY);
#elif TIMER1_CONFIG == PWM_OCR1AB
	extern void INIT_TIMER1 (uint8_t duty_A, uint8_t duty_B);
#endif
#if TIMER1_CONFIG != OFF
	extern void TIMER1_OFF();
#endif

#endif /* TIMER_H_ */
