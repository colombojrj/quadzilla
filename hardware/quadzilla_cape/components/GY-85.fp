
Element["" "" "" "" 87.9389mm 31.8732mm 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 0.0000 36.00mil 20.00mil 42.00mil 20.00mil "" "1" "edge2"]
	Pin[100.00mil -0.39mil 36.00mil 20.00mil 42.00mil 20.00mil "" "2" "edge2"]
	Pin[199.60mil -0.39mil 36.00mil 20.00mil 42.00mil 20.00mil "" "3" "edge2"]
	Pin[300.00mil -0.39mil 36.00mil 20.00mil 42.00mil 20.00mil "" "4" "edge2"]
	Pin[499.21mil 0.0000 36.00mil 20.00mil 42.00mil 20.00mil "" "6" "edge2"]
	Pin[399.60mil -0.79mil 36.00mil 20.00mil 42.00mil 20.00mil "" "5" "edge2"]
	Pin[599.13mil 0.0000 36.00mil 20.00mil 42.00mil 20.00mil "" "7" "edge2"]
	Pin[699.14mil 0.17mil 36.00mil 20.00mil 42.00mil 20.00mil "" "8" "edge2"]
	Pin[670.28mil 481.04mil 3.2000mm 20.00mil 3.3524mm 3.0000mm "" "9" "edge2"]
	ElementLine [-62.16mil 595.15mil -62.16mil -74.85mil 10.00mil]
	ElementLine [-62.16mil -74.71mil 784.55mil -74.71mil 10.00mil]
	ElementLine [784.55mil -74.71mil 784.55mil 15.1110mm 10.00mil]
	ElementLine [784.55mil 15.1110mm -62.16mil 15.1110mm 10.00mil]

	)
