%constructing the matrices of the state space model 
function [A,B,C,D] = givemeABCD(Km,Kt,mq,w0,dcm,Ixx,Iyy,Izz,g,rx,ry,rz)
% X = [W P Q R Z PHI THETA PSI] 
A = [zeros(4,8);
     1 zeros(1,7);
     0 1 zeros(1,6);
     0 0 1 zeros(1,5);
     0 0 0 1 zeros(1,4)];

B = [-2*Kt*w0(1)/mq        -2*Kt*w0(2)/mq    -2*Kt*w0(3)/mq      -2*Kt*w0(4)/mq ;
     0                     2*w0(2)*Kt*dcm/Ixx    0               -2*w0(4)*Kt*dcm/Ixx; 
     2*w0(1)*Kt*dcm/Iyy         0            -2*w0(3)*Kt*dcm/Iyy   0;
     2*w0(1)*Km/Izz        -2*w0(2)*Km/Izz    2*w0(3)*Km/Izz     -2*w0(4)*Km/Izz;
     zeros(4,4)];

W = diag(w0);

C = zeros(8, 8);
C(1,2) =   1;
C(2,3) =   1;
C(3,4)  =  1;
C(4,7)  = -g;
C(5,6)  =  g;
C(7,8)  =  1;
C(8,5)  =  1;

%
% 
 D = zeros(8, 4);
 D(4,1) = 2*Kt*dcm*rz/Iyy - 2*Km*ry/Izz;
 D(4,2) = 2*Km*ry/Izz;
 D(4,3) = -2*Km*ry/Izz -2*Kt*dcm*rz/Iyy;
 D(4,4) = 2*Km*ry/Izz;
 D(5,1) = 2*Km*rx/Izz;
 D(5,2) = -2*Km*rx/Izz -2*Kt*dcm*rz/Ixx;
 D(5,3) = 2*Km*rx/Izz;
 D(5,4) = -2*Km*rx/Izz + 2*Kt*dcm*rz/Ixx;
 D(6,1) = -2*Kt*dcm*rx/Iyy;
 D(6,2) = 2*Kt*dcm*ry/Ixx;
 D(6,3) =  2*Kt*dcm*rx/Iyy;
 D(6,4) =  -2*Kt*dcm*ry/Ixx;
 D = D*W;
%  
%  %X = [U V W P Q R X Y Z PHI THETA PSI] 
% % A = zeros(12,12);
% % for i=7:12
% %     A(i,i-6) = 1;
% % end
% % A(1,11) = -g;
% % A(2,10) =  g; 
% % B = [zeros(2, 4) ; 
% %     -2*Kt*w0(1)/mq        -2*Kt*w0(2)/mq    -2*Kt*w0(3)/mq      -2*Kt*w0(4)/mq ;
% %      0                     -2*w0(2)*Kt*dcm/Ixx    0               2*w0(4)*Kt*dcm/Ixx; %the signal is different from the Domingues work's
% %      2*w0(1)*Kt*dcm/Iyy         0            -2*w0(3)*Kt*dcm/Iyy   0;
% %      2*w0(1)*Km/Izz        -2*w0(2)*Km/Izz    2*w0(3)*Km/Izz     -2*w0(4)*Km/Izz;
% %      zeros(6,4)];


% %%tentar nao mata
% C = eye(8,8);
% C(7,7) = 0;
% D = D*0;
