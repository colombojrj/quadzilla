function [sys, x0, str, ts] = sSystemSensors(t, x, u, flag)
%
% Planta integrador duplo implementado em S-Functioin
% Authors:
% * Luis Arthur Gagg Filho
% * Jos� Roberto Colombo Junior
% * Pedro Augusto Queiroz de Assis
%

switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes();

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Update continuous states %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %case 1,
  %  sys=mdlDerivatives(t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs(t,u);

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  case { 1, 2, 4, 9 } % Unused flags
    sys = [];
  otherwise
    DAStudio.error('Simulink:blocks:unhandledFlag', num2str(flag));

end

% end sfuntmpl

%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes()

%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
sizes = simsizes;

sizes.NumContStates  = 0; 
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 4;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes(sizes);

x0  = []; % initialize the initial conditions
ts  = [0 0];      % sample time: [period, offset]
str = [];
% end mdlInitializeSizes

%
%=============================================================================
% mdlUpdate
% Update discrete states
%=============================================================================
%
function sys=mdlDerivatives(t, x, u)

sys = quat2euler(u);
% end mdlDerivatives

%
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
%
function sys=mdlOutputs(t,q)
sys = quat2euler(q);
% end mdlOutputs
