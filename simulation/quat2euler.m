function Euler_Angles = quat2euler(q)
%
% This function returns the Euler Angles (Phi, Theta, Psi) in radians
%
% 10-10-2015
%
% Authors:
% * Luis Arthur Gagg Filho
% * Jos� Roberto Colombo Junior
% * Pedro Augusto Queiroz de Assis
%
% Inputs
% q = a + b i + c j + d k
% where i�j = k, j�i = ?k, j�k = i, k�j = ?i, k�i = j, i�k = ?j, i�i = j�j = k�k = ?1
%
%Outputs
% Euler_Angles = [Phi Theta Psi]' in rad

Phi =  atan2(q(1)*q(3)+q(2)* q(4),q(1)*q(4)-q(2)*q(3));
% Theta= asin(-q(1)^2-q(2)^2+q(3)^2+q(4)^2);
Theta= asin(2*q(1)*q(2)+2*q(3)*q(4));
Psi =  atan2(q(1)*q(3)-q(2)*q(4),q(2)*q(3)+q(1)*q(4)); 

% Reference: http://math.stackexchange.com/questions/687964/getting-euler-tait-bryan-angles-from-quaternion-representation

Euler_Angles = [Phi Theta Psi]';

end