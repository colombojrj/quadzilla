function [sys, x0, str, ts] = SystemNonLinearModel(t, x, u, flag, Kt , dcm,  Ktm, mq, I)
%
% Planta integrador duplo implementado em S-Functioin
% Authors:
% * Luis Arthur Gagg Filho
% * Jos� Roberto Colombo Junior
% * Pedro Augusto Queiroz de Assis
%

% loadvars;
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes();

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Update continuous states %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives(t,x,u, Kt , dcm,  Ktm, mq, I);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs(t, x);

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 } % Unused flags
    sys = [];
  otherwise
    DAStudio.error('Simulink:blocks:unhandledFlag', num2str(flag));

end

% end sfuntmpl

%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes()

%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
sizes = simsizes;

sizes.NumContStates  = 13; 
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 13;
sizes.NumInputs      = 4;
sizes.DirFeedthrough = 0;
sizes.NumSampleTimes = 1;

sys = simsizes(sizes);

% x = [U V W P Q R X Y  Z q1 q2 q3 q4]
x0  = [0 0 0 0 0 0 0 0 -1  1  0  0  0];
ts  = [0 0];      % sample time: [period, offset]
str = [];
% end mdlInitializeSizes

%
%=============================================================================
% mdlUpdate
% Update discrete states
%=============================================================================
%
function sys=mdlDerivatives(t, x, u, Kt , dcm,  Ktm, mq, I)

sys = NonLinearModel(x,u, Kt , dcm,  Ktm, mq, I);
% end mdlDerivatives

%
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
%
function sys=mdlOutputs(t,x)

%     derivatives = NonLinearModel( x, u, Kt , dcm,  Ktm, mq, I);

    sys = x;
% end mdlOutputs
