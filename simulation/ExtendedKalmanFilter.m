function [sys, x0, str, ts] = ExtendedKalmanFilter(t, x, inputs, flag, Ts, Pw, Pv,Ad,Bd,Cd,Dd,P0)
%
% Extended Kalman Filter implemented in S-Function
%
% Inputs:
% - Q: covariance weighting matrix
% - R: unknown name matrix
% - Pk: covariance matrix
%
%
% Authors:
% * Arthur...
% * Jos� Roberto Colombo Junior
% * Pedro...
%

switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts] = mdlInitializeSizes(Ad,Bd,Cd,P0,Ts);

  %%%%%%%%%%%%%%%%%%%%%%%%%%
  % Update discrete states %
  %%%%%%%%%%%%%%%%%%%%%%%%%%
  case 2,
    sys = mdlUpdate(t);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys = mdlOutputs(t,inputs,x,Pw,Pv,Ad,Bd,Cd,Dd);

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  case { 1, 4, 9 } % Unused flags
    sys = [];
  otherwise
    DAStudio.error('Simulink:blocks:unhandledFlag', num2str(flag));

end

% end sfuntmpl

%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts] = mdlInitializeSizes(Ad,Bd,Cd,P0,Ts)

global nx ny
%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
sizes = simsizes;

% Useful variables
nx = size(Ad,1);
nu = size(Bd,2);
ny = size(Cd,1);

sizes.NumContStates  = 0; 
sizes.NumDiscStates  = nx*nx+nx; %?
sizes.NumOutputs     = nx; %k
sizes.NumInputs      = ny+nu; %k
sizes.DirFeedthrough = 0;
sizes.NumSampleTimes = 1;

sys = simsizes(sizes);

x0  = [zeros(nx,1) P0]; % initialize the initial conditions

ts  = [Ts 0];                % sample time: [period, offset]
str = [];
% end mdlInitializeSizes

%
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
%
function sys = mdlOutputs(t,inputs,x,Pw,Pv,Ad,Bd,Cd,Dd)

global nx ny xcp Pk
    
    % Inputs
    y = inputs(1:ny);
    u = inputs(ny+1:end);
    
    
    
    % Update covariance matrix
    %P(k|k-1)
    Pk_ = Ad*Pk*Ad' + Pw;
    % Kb(k) = P(k|k-1)C'(C P(k|k-1)C'+Pv)^{-1}
    Kk = Pk_*Cd' * (Cd*Pk_*Cd' +Pv)^-1;
    
    % Estimate output
    % y(k|k-1) - C x(k|k-1)
    yc = Cd*xcp;
    
    % Measurement update (inovation)
    % xc(k|k) = xc(k|k-1) + Kb (y(k)-yc(k|k-1))
    xc = xcp + Kk*(y -yc -Dd*u);
    
    % Control applied at k
    %propaga��o
    xcp = Ad*xc + Bd*u; % x(k+1|k) = A*x(k|k) + B*u(k)
    Pk = (eye(nx,nx) -Kk*Cd)*Pk_;

    sys = xc;
% end mdlOutputs
%
%=============================================================================
% mdlUpdate
% Update discrete states
%=============================================================================
%
function sys = mdlUpdate(t)
global  xcp Pk
sys = [xcp Pk];
