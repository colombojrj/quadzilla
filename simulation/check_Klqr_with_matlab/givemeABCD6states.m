%constructing the matrices of the state space model 
function [A,B,C,D] = givemeABCD6states(Km,Kt,w0,dcm,Ixx,Iyy,Izz,g)
% X = [W P Q R Z PHI THETA PSI] 
A = [zeros(3,6); 1 zeros(1,5); 0 1 zeros(1,4); 0 0 1 zeros(1,3);];

fx = 2*Kt*w0*dcm/Ixx;
fy = 2*Kt*w0*dcm/Iyy;
fz = 2*Km*w0*dcm/Izz;
B = [ 0 fx 0 -fx; fy 0 -fy 0; fz -fz fz -fz; zeros(3,4)];

W = diag([w0 w0 w0 w0]);

C = zeros(7, 6);
C(1,1) =   1;
C(2,2) =   1;
C(3,3)  =  1;
C(4,4)  =  1;
C(5,5)  =  1;
C(6,6)  =  1;

%
% 
 D = zeros(7, 4);
