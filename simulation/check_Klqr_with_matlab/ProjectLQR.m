%
% Project LQR
%

clc, clear, close all

% Load quadcopter parameters
ourloadvars;

% Construct linear model
[A,B,C,D] = givemeABCD6states(Km,Kt,w0,dcm,Ixx,Iyy,Izz,g);
% X = [P Q R phi theta psi]

% Discretize system model
Ts = 0.100;
[Ad,Bd,Cd,Dd] = c2dm(A,B,C,D,Ts);

% Project LQR controller

Q = 0.1*eye(size(A,1));
R = 0.1*eye(size(B,2));
K = dlqr(Ad, Bd, Q, R);

Cd = Cd(1:end-1,:);
Qobs = 0.1*eye(size(A,1));
Robs = 0.1*eye(size(C',2));
%Kobs = dlqr(Ad',Cd', Qobs, Robs);


sdv = [0.0319 0.0356  0.034 0.412 0.412 0.412];
Pv = sdv' * sdv;
Pw = eye(6)*10^-5;
P = eye(6)*10^-3; %P0

for i = 1:500
  Kobs = (Ad*P*Cd' + Pw)*((Cd*P*Cd' + Pv)^-1);
  dummy{i} = Kobs;
  P = Ad*P*Ad' - (Ad*P*Cd')*((Cd*P*Cd' + Pv)^-1)*(Ad*P*Cd')' + Pw;
end