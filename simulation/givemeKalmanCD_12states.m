%constructing the matrices of the state space model 
function [C,D] = givemeKalmanCD_12states(Km,Kt,mq,w0,dcm,Ix,Iy,Iz,g)
% X = [P Q R PHI THETA PSI] 

C = zeros(6,12);
C(1,11) = -g;
C(2,10) =  g;
C(3,4)  =  1;
C(4,5)  =  1;
C(5,6)  =  1;
C(6,12) = -1;

D = zeros(6,4);