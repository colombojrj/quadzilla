function [ Ad, Bd, Cd, Dd ] = GiveMeLinearABCD( Ts, xeq, ueq )
%
% This function returns a discrete linear model of a quadcopter
%
% 09-10-2015
% Authors:
% * Luis Arthur Gagg Filho
% * Jos� Roberto Colombo Junior
% * Pedro Augusto Queiroz de Assis1
%

%% Quadcopter model with inputs being the measurements of gyro
% State vector X = [Phi Theta Psi] - Euler angles
% Input vector U = [P Q R] - angular velocities
% It is assumed that the gyros measurements are already the correct states.

Ad = eye(3);
Bd = eye(3)*Ts;
Cd = [0    -9.81  0;
         9.81   0    0;
         zeros(3);
         0       0  -cos(xeq(3)) ]; 
     %note: xeq(3)(k|k-1)
 Dd = [zeros(2,3);
          eye(3);
          0 0 0];


%% Double Integrator
% Ad = [1 1 ; 0 Ts];
% Bd = [0 ; Ts];
% Cd = [1 0];
% Dd = 0;

end