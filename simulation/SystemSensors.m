function [bar_Omega bar_a bar_N] = SystemSensors( Euler_Angles, Linear_Velocities )
%
% This function simulates the dynamic of the sensors:
% acceletometer, gyroscope and magnetometer
%
% 10-10-2015
%% Authors:
% * Luis Arthur Gagg Filho
% * Jos� Roberto Colombo Junior
% * Pedro Augusto Queiroz de Assis
%
%% Inputs:
% 1- Euler Angles: phi, theta, psi (in radians)
% 2- Linear Velocities in body frame: P,Q, R
% 3 - Aceleration in body frame: dP, dQ, dR (CONFERIR)
% Constants
% 4 - rz: heigh from the acceletometer to the center of mass of the quad,
% ry = rx = 0
% 5 - mu_a = gaussian noise of acceletometer, note: bias of acceletometer
% is neglected
% 6 - mu_m = gaussian noise of magnetometer
% 7 - b_m = bias of magnetometer
% 8 - mu_g = gaussian noif of gyroscope
% 9 - b_g = bias of gyroscope
%10 - g = gravity
%not%e 5 -- 9 are (3x1) vectors

%% Outpus
% Return the measurements of the sensors, 3x1 vectors, gyroscope, acceletometer and magnetometer

%% Gyroscope
%Equation (4.2)
Omega = [P; Q; R];
bar_Omega = Omega + mu_g + b_g;

%% Acceletometer
%Equation (4.13)
bar_ax = -g*sin(theta) + dQ*rz + R*P*rz;
bar_ay = g*cos(theta)*sin(phi) - dP*rz + R*Q*rz;
bar_az = g*cos(theta)*cos(phi) - P*P*rz-Q*Q*rs;

bar_a = [bar_ax ; bar_ay ; bar_az]
%% Magnetometer
% Equation (4.15) (Perhaps I shoud use the equation (4.17))
bar_N = [cos(theta)*cos(psi); cos(psi)*sin(phi)*sin(theta) - cos(phi)*sin(phi); cos(psi)*cos(phi)*sin(theta) + sin(phi)*sin(psi)] + mu_m + b_m;

end