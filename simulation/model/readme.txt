Pedro,

Para abrir as equações do modelo, você precisará de:
- um computador
- ipython
- saco

Primeiro, instalei o ipython:
sudo apt-get install ipython ipython-notebook -y

Depois, rode os comandos no terminal:
cd (alguma coisa)/quadcoptero/modelo
ipython notebook