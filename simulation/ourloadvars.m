% NOSSO LOADVARS

%Description: This m-file initializes all necessary variables required to
%buid the dynamic model of a quadrotor in the state-space form.

%clear all;
%% Physical properties of the environment
g = 10.05; %Acceleration of gravity in SJC
rho = 1.2; %Density of air (m^3.kg^-1)

%% Physical properties of the quadrotor
mq = 1.75; %Total mass of the quadrotor

mm = 0.039; %Mass of a motor (kg). All motors have equal mass.
lx = 28.5e-3; %Motor length along x-axis (m). All motors have equal sizes.
ly = 28.5e-3; %Motor length along y-axis (m)
lz = 30.6e-3; %Motor length along z-axis (m)
dcm = 0.2975; %Distance from the center of gravity to the center of a motor (m).
%The quadrotor is symmetric regarding the XZ and YZ planes, so
%dcm is the same for all motors.

Ix1 = (1/12)*mm*(ly^2+lz^2); %Moment of inertia (x-axis) for motors 1 and 3
%(kg.m^2).
Ix2 = (1/12)*mm*(ly^2+lz^2)+mm*dcm^2; %Moment of inertia (x-axis) for motors
%2 and 4 (kg.m^2).
Ixx = 2*Ix1+2*Ix2; %Total moment of inertia along the x-axis (kg.m^2)
Iy1 = (1/12)*mm*(lx^2+lz^2)+mm*dcm^2; %Moment of inertia (y-axis) for motors
%1 and 3 (kg.m^2).
Iy2 = (1/12)*mm*(lx^2+lz^2); %Moment of inertia (y-axis) for motors 2 and 4
%(kg.m^2).
Iyy = 2*Iy1+2*Iy2; %Total moment of inertia along the y-axis (kg.m^2)
Iz1 = (1/12)*mm*(lx^2+ly^2)+mm*dcm^2; %Moment of inertia (z-axis) for motors
%1 and 3 (kg.m^2).
Iz2 = (1/12)*mm*(lx^2+ly^2)+mm*dcm^2; %Moment of inertia (z-axis) for motors
%2 and 4 (kg.m^2).
Izz = 2*Iz1+2*Iz2; %Total moment of inertia along the z-axis (kg.m^2)
I = diag([Ixx Iyy Izz]); %Inertia matrix

% FONTE PESSOAL DO LAB DO DAVI
Km = 7.5e-7; %Constant value to calculate the moment provided
%by a propeller given its angular speed (kg.m^2.rad^-1)
Kt = 3.13e-5; %Constant value to calculate the thrust provided
%by a propeller given its angular speed (kg.m.rad^-1)

Ktm = Km/Kt; %Constant that relates thrust and moment of a propeller.

%% Some parameters of the motors

% For equation PWM = k*w + offset and w = (PWM -offset)/k
motor1_k = 37.1834;
offset1 = -2102.5553;
motor2_k = 16.5808;
offset2 = -738.7423;
motor3_k = 17.7869;
offset3 = -835.7571;
motor4_k = 19.1677;
offset4 = -880.5494;
tau = 1/((6.0763 + 5.2313 + 4.7891 + 4.5531)/4);

%% Initial states
% w0 = [371.8 371.8 371.8 371.8];
w0 = [371.13 371.13 371.13 371.13];


%% Sensors informations
gyro_b_x = -4.993*0;
gyro_b_y = 2.8406*0;
gyro_b_z = 1.058*0;
gyro_var_x  = 0.0319^2*0;
gyro_var_y  = 0.0356^2*0;
gyro_var_z  = 0.034^2*0;

acc_b_x = 0.144*0;
acc_b_y = -0.0332*0;
acc_b_z = 0.0109*0;
acc_var_x  = 0.0379^2*0;
acc_var_y  = 0.0412^2*0;
acc_var_z  = 0.0455^2*0;

rx = 9e-3*0;
ry = 12e-3*0;
% rz = -57e-3;
rz = 0.1;

compass_b = 0.0;
compass_var = 0.00;

barometer_var  = 0.4157^2*0;
local_altitude = 660; %660 %588.52