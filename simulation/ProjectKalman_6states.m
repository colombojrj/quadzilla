%
% Project LQR
%

% Load quadcopter parameters
loadvars;

% Construct linear model
[A,B,C,D] = givemeKalmanAC_6states(Km,Kt,mq,w0,dcm,Ixx,Iyy,Izz,g);

% Discretize system model
Ts = 0.050;
[Ad,Bd,Cd,Dd] = c2dm(A,B,C,D,Ts);

% Project LQR controller
Q = diag([0.1 0.1 0.1 0.1 0.1 0.1]);
R = diag([1,1,1]);
L = dlqr(Ad', Cd', Q, R)';


