%constructing the matrices of the state space model 
function [A,B,C,D] = givemeKalmanAC_6states(Km,Kt,mq,w0,dcm,Ixx,Iyy,Izz,g,rx,ry,rz)
% X = [P Q R PHI THETA PSI] 

W = diag(w0);

A = zeros(6,6);
A(4,1) = 1;
A(5,2) = 1;
A(6,3) = 1;
B = zeros(6,4);
B(1,2) = -2*Kt*dcm/Ixx;
B(1,4) =  2*Kt*dcm/Ixx;
B(2,1) =  2*Kt*dcm/Iyy;
B(2,3) = -2*Kt*dcm/Iyy;
B(3,1) =  2*Km/Izz;
B(3,2) = -2*Km/Izz;
B(3,3) =  2*Km/Izz;
B(3,4) = -2*Km/Izz;
B = B*W;

C = zeros(9, 12);
C(1,11) = -g;
C(2,10) =  g;
C(4,4)  =  1;
C(5,5)  =  1;
C(6,6)  =  1;
C(8,12) = -1;
C(9,9)  =  1;

D = zeros(9, 4);
D(1,1) = 2*Kt*dcm*rz/Iyy -2*Km*ry/Izz;
D(1,2) = 2*Km*ry/Izz;
D(1,3) = -2*Km*ry/Izz -2*Kt*dcm*rz/Iyy;
D(1,4) = 2*Km*ry/Izz;
D(2,1) = 2*Km*rx/Izz;
D(2,2) = -2*Km*rx/Izz +2*Kt*dcm*rz/Ixx;
D(2,3) = 2*Km*rx/Izz;
D(2,4) = -2*Km*rx/Izz -2*Kt*dcm*rz/Ixx;
D(3,1) = -2*Kt*dcm*rx/Iyy;
D(3,2) = -2*Kt*dcm*ry/Ixx;
D(3,3) =  2*Kt*dcm*rx/Iyy;
D(3,4) =  2*Kt*dcm*ry/Ixx;
D = D*W;


