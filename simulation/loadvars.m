% descobrindo avlor de equilíbrio controles
% RETIRADO DE DOMINGUES APENDICE

%Description: This m-file initializes all necessary variables required to
%buid the dynamic model of a quadrotor in the state-space form.

%clear all;
%% Physical properties of the environment
g = 9.81; %Acceleration of gravity (m)
rho = 1.2; %Density of air (m^3.kg^-1)

%% Physical properties of the quadrotor
mq = 0.82; %Total mass of the quadrotor
mm = 0.048; %Mass of a motor (kg). All motors have equal mass.
lx = 28.8e-3; %Motor length along x-axis (m). All motors have equal sizes.
ly = 28.8e-3; %Motor length along y-axis (m)
lz = 26e-3; %Motor length along z-axis (m)
dcm = 0.29; %Distance from the center of gravity to the center of a motor (m).
%The quadrotor is symmetric regarding the XZ and YZ planes, so
%dcm is the same for all motors.
Ix1 = (1/12)*mm*(ly^2+lz^2); %Moment of inertia (x-axis) for motors 1 and 3
%(kg.m^2).


Ix2 = (1/12)*mm*(ly^2+lz^2)+mm*dcm^2; %Moment of inertia (x-axis) for motors
%2 and 4 (kg.m^2).
Ixx = 2*Ix1+2*Ix2; %Total moment of inertia along the x-axis (kg.m^2)
Iy1 = (1/12)*mm*(lx^2+lz^2)+mm*dcm^2; %Moment of inertia (y-axis) for motors
%1 and 3 (kg.m^2).
Iy2 = (1/12)*mm*(lx^2+lz^2); %Moment of inertia (y-axis) for motors 2 and 4
%(kg.m^2).
Iyy = 2*Iy1+2*Iy2; %Total moment of inertia along the y-axis (kg.m^2)
Iz1 = (1/12)*mm*(lx^2+ly^2)+mm*dcm^2; %Moment of inertia (z-axis) for motors
%1 and 3 (kg.m^2).
Iz2 = (1/12)*mm*(lx^2+ly^2)+mm*dcm^2; %Moment of inertia (z-axis) for motors
%2 and 4 (kg.m^2).
Izz = 2*Iz1+2*Iz2; %Total moment of inertia along the z-axis (kg.m^2)
I = diag([Ixx Iyy Izz]); %Inertia matrix

cp = 0.0743; %Thrust coefficient of the propeller
ct = 0.1154; %Power coefficient
rp = .5*25.4e-2; %Propeller radius (m)

Km = cp*4*rho*rp^5/pi()^3; %Constant value to calculate the moment provided
%by a propeller given its angular speed (kg.m^2.rad^-1)
Kt = ct*4*rho*rp^4/pi()^2; %Constant value to calculate the thrust provided
%by a propeller given its angular speed (kg.m.rad^-1)

Ktm = Km/Kt; %Constant that relates thrust and moment of a propeller.
k1 = 2.028; %Transfer-function gain of motor 1
k2 = 1.869; %Transfer-function gain of motor 2
k3 = 2.002; %Transfer-function gain of motor 3
k4 = 1.996; %Transfer-function gain of motor 4
tc = 0.136; %Time-constant of the motors (assumed equal for all motors)
dz1 = 1247.4; %PWM dead-zone of motor 1 given its first-order transfer
%function (micro-seconds)
dz2 = 1247.8; %PWM dead-zone of motor 2 given its first-order transfer
%function (micro-seconds)
dz3 = 1246.4; %PWM dead-zone of motor 3 given its first-order transfer
%function (micro-seconds)
dz4 = 1247.9; %PWM dead-zone of motor 4 given its first-order transfer

%function (micro-seconds)
g_m1 = tf(k1,[tc 1]); %First-order transfer-function of motor 1
g_m2 = tf(k2,[tc 1]); %First-order transfer-function of motor 2
g_m3 = tf(k3,[tc 1]); %First-order transfer-function of motor 3
g_m4 = tf(k4,[tc 1]); %First-order transfer-function of motor 4
%Note: If the behavior of the motors is not linear, linearization around
%an operating point is required to attain these transfer-functions.
PWM_max = 2200; %Upper limit of the PWM signal provided to
%the motors (micro-seconds)

%% Sensors
daccel_x  =  0.003; %Accelerometer distance to the quadrotor?s center of
%gravity along the x-axis (m)
daccel_y  =  0; %Accelerometer distance to the quadrotor?s center of
%gravity along the y-axis (m)
daccel_z  =  -0.0490; %Accelerometer distance to the quadrotor?s center of
%gravity along the z-axis (m)
daccel = [daccel_x daccel_y daccel_z];
accel_max  =  3*g; %Accelerometer maximum possible reading
accel_min  =  -3*g; %Accelerometer minimum possible reading
cres  =  1*pi()/180;
%Compass reading resolution (rad)  =  i degree x 180 degrees / pi() rad.
%Note: this can be configured in the Arduino to be either 1 or 0.5 degrees
%% Initial states
k1  =  2.0276; %Constant that relates Rotation speed of propeller 1 with the
%corresponding PWM pulse of the Arduino
k2  =  1.8693; %Constant that relates Rotation speed of propeller 2 with the

%corresponding PWM pulse of the Arduino
k3  =  2.0018; %Constant that relates Rotation speed of propeller 3 with the
%corresponding PWM pulse of the Arduino
k4  =  1.9986; %Constant that relates Rotation speed of propeller 4 with the
%corresponding PWM pulse of the Arduino
% PWMdz1  =  1256; %Dead zone of the motor 1 (PWM pulse-width in microseconds)
% PWMdz2  =  1264; %Dead zone of the motor 2 (PWM pulse-width in microseconds)
% PWMdz3  =  1256; %Dead zone of the motor 3 (PWM pulse-width in microseconds)
% PWMdz4  =  1256; %Dead zone of the motor 4 (PWM pulse-width in microseconds)
%Calculate the PWM pulse necessary to beat the force of gravity
% syms PWMg1 PWMg2 PWMg3 PWMg4
% PWMg1 = solve((PWMg1-PWMdz1)^2-(1/4)*mq*g/(Kt*(k1^2)),PWMg1);
% PWMg1 = double(PWMg1);
% PWMg1 = PWMg1(PWMg1>PWMdz1);
% PWMg2 = solve((PWMg2-PWMdz2)^2-(1/4)*mq*g/(Kt*(k2^2)),PWMg2);
% PWMg2 = double(PWMg2);
% PWMg2 = PWMg2(PWMg2>PWMdz2);
% PWMg3 = solve((PWMg3-PWMdz3)^2-(1/4)*mq*g/(Kt*(k3^2)),PWMg3);
% PWMg3 = double(PWMg3);
% PWMg3 = PWMg3(PWMg3>PWMdz3);
% PWMg4 = solve((PWMg4-PWMdz4)^2-(1/4)*mq*g/((Kt*k4^2)),PWMg4);
% PWMg4 = double(PWMg4);
% PWMg4 = PWMg4(PWMg4>PWMdz4);
% % %Compute the necessary rotation speed (rad.s^-1) of each propeller to beat
% % %the force of gravity
% wm1  =  double(k1*(PWMg1-PWMdz1));
% wm2  =  double(k2*(PWMg2-PWMdz2));
% wm3  =  double(k3*(PWMg3-PWM0dz3));
% wm4  =  double(k4*(PWMg4-PWMdz4));
% w0 = [wm1 wm2 wm3 wm4]; %Initial angular speed of motors 1 to 4 (rad.s^-1)
w0 = [371.1337  371.1337  371.1337  371.1337];





% Sensor information
gyro_bias_x = 0.0;
gyro_bias_y = 0.0;
gyro_bias_z = 0.0;
gyro_var_x  = 0.001;
gyro_var_y  = 0.001;
gyro_var_z  = 0.001;

acc_bias_x = 0.0;
acc_bias_y = 0.0;
acc_bias_z = 0.0;
acc_var_x  = 0.00;
acc_var_y  = 0.00;
acc_var_z  = 0.00;

rx = 0;
ry = 0;
rz = 0.10;

compass_bias_x = 0.0;
compass_bias_y = 0.0;
compass_bias_z = 0.0;
compass_var_x  = 0.00;
compass_var_y  = 0.00;
compass_var_z  = 0.00;


barometer_var  = 0.00;
local_altitude = 660;