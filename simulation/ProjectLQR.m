%
% Project LQR
%

clc, clear, close all

% Load quadcopter parameters
ourloadvars;

% Construct linear model
[A,B,C,D] = givemeABCD(Km,Kt,mq,w0,dcm,Ixx,Iyy,Izz,g,rx,ry,rz);

% Discretize system model
Ts = 0.100;
[Ad,Bd,Cd,Dd] = c2dm(A,B,C,D,Ts);

% Project LQR controller

Q = 3*eye(8);
R = diag((1/3^2)*[1,1,1,1])*10^-1;
K = dlqr(Ad, Bd, Q, R);

% Kalman filter project
P = eye(8)*10^-3;
Pw = eye(8)*10^-9;
Pv = eye(8)*10^-7;
for i =1:10000
    P = Ad*P*Ad'+Pw;
    Kb = P*Cd'*(Cd*P*Cd'+Pv)^-1;
    P = (eye(8)-Kb*Cd)*P;
end

% References
zref = 2*0;
psiref = 1.5*0;

%initial condition
z0 = 2; x0 = 0; y0 = 0;
P0 = 0; Q0 =0; R0 = 0;
U0 = 0; V0 =0; W0 = 0;
q00 = 1; q10 = 0; q20 = 0; q30 = 0;
% initial estimates
%remeber X = [W P Q R Z PHI THETA PSI]
xc0 = [0 0 0 0 0 0 0 0];

% height linearization point
Z0lin = 2;