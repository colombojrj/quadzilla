# README #

### This project intends to create a set of libraries to help managing a QuadCopter project.

We know that people may use different onboard computers (for example BeagleBone White and Black, Raspberry Pi and others). So, we tryed to use only python native libraries (motors library is exception, but it can be easily adapted to your needs).

There are libraries to support:
* sensors (accelerometers, magnetometers, gyroscopes, and more)
* motors (controlling the ESCs directly)
* Kalman filter (soon!)
* and more!

### How do I get set up? ###
* Onboard computer with ARM processor (we used BeagleBone White)
* Preferably Debian Wheezy
* Numpy, Scipy, Matplotlib, Python SMBUS

### Who do I talk to? ###

* Soon!