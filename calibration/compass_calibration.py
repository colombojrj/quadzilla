import compass, accelerometer
import smbus, time
from scipy.io import savemat
from numpy import zeros

# Start I2C communication
bus = smbus.SMBus(1)

# Initialize the sensors
mag = compass.HCM5883(bus)
acc = accelerometer.BMA180(bus)

# Delay and execution time
tend = 120
delay = 10e-3

# Initialize the data array
mag_data = zeros((3,int(tend/delay)))
acc_data = zeros((3,int(tend/delay)))

print "Started the experiment"

for k in xrange(0,int(tend/delay)):
  acc_aux = acc.read()
  mag_aux = mag.read()
  acc_data[:,k] = acc_aux.squeeze()
  mag_data[:,k] = mag_aux.squeeze()
  
  time.sleep(delay)

savemat('3d_compass_calibration.mat', {'mag_data':mag_data,'acc_data':acc_data})
bus.close()

