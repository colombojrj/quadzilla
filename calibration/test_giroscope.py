import accelerometer, compass, gyroscope
import smbus, time
from scipy.io import savemat
from numpy import zeros

# Start I2C communication
bus = smbus.SMBus(1)

# Initialize the sensors
mag = compass.HCM5883(bus, REMOVE_OFFSET=False)
gyro= gyroscope.ITG_3200(bus)
acc = accelerometer.BMA180(bus)

# Delay and execution time
tend = 60
delay = 10e-3

# Initialize the data array
mag_data   = zeros((3,int(tend/delay)))
gyro_data  = zeros((3,int(tend/delay)))
pitch_data = zeros((1,int(tend/delay)))
roll_data  = zeros((1,int(tend/delay)))

for k in xrange(0,int(tend/delay)):
  gyro_aux = gyro.read()
  mag_aux  = mag.read()
  pitch, roll  = acc.pitchroll()
  for i in xrange(0,3):
    gyro_data[i,k]= gyro_aux[i]
    #mag_data[i,k] = mag_aux[i]
  pitch_data[0,k] = pitch
  roll_data[0,k] = roll
  
  time.sleep(delay)

#savemat('matlab_data.mat', {'mag_data':mag_data,'gyro_data':gyro_data})
savemat('matlab_data.mat', {'pitch':pitch_data,'roll':roll_data,'gyro':gyro_data})
bus.close()

