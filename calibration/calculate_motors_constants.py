# -*- coding: utf-8 -*-

import numpy as np
import pylab as plt
from scipy.optimize import curve_fit

# Create the dictionaries to store data
M1 = dict()
M2 = dict()
M3 = dict()
M4 = dict()

# Load data to the dictionaries
M1['LENGTH'] = np.array([1150, 1200, 1250, 1300, 1350]) # in us
M1['PULSES'] = np.array([  88,  128,  172,  207,  232])
M2['LENGTH'] = np.array([1150, 1200, 1250, 1300, 1350]) # in us
M2['PULSES'] = np.array([  87,  126,  165,  198,  227])
M3['LENGTH'] = np.array([1150, 1200, 1250, 1300, 1350, 1400]) # in us
M3['PULSES'] = np.array([  88,  126,  166,  200,  229,  256])
M4['LENGTH'] = np.array([1150, 1200, 1250, 1300, 1350]) # in us
M4['PULSES'] = np.array([  82,  126,  165,  200,  230])

# Convert pulse to rad/s
def PULSE2RAD (DATA, constant):
  return DATA * constant
constant = np.pi / 2
M1['w'] = PULSE2RAD (M1['PULSES'], constant)
M2['w'] = PULSE2RAD (M2['PULSES'], constant)
M3['w'] = PULSE2RAD (M3['PULSES'], constant)
M4['w'] = PULSE2RAD (M4['PULSES'], constant)

# Fit curves
def w2LENGTH(w, ka, offset):
    return ka*w +offset

popt, pcov = curve_fit(w2LENGTH, M1['w'], M1['LENGTH'])
M1['ka'] = popt[0]
M1['offset'] = popt[1]
popt, pcov = curve_fit(w2LENGTH, M2['w'], M2['LENGTH'])
M2['ka'] = popt[0]
M2['offset'] = popt[1]
popt, pcov = curve_fit(w2LENGTH, M3['w'], M3['LENGTH'])
M3['ka'] = popt[0]
M3['offset'] = popt[1]
popt, pcov = curve_fit(w2LENGTH, M4['w'], M4['LENGTH'])
M4['ka'] = popt[0]
M4['offset'] = popt[1]

print

# Plot data
plt.figure()
# Motor 1
plt.subplot(2, 2, 1)
plt.plot(M1['w'], M1['LENGTH'], '-*g', label='Motor 1 measured')
plt.plot(M1['w'], w2LENGTH(M1['w'], M1['ka'], M1['offset']), 'b', label='Motor 1 fitted')
plt.grid(True)
plt.legend(loc='best')
plt.ylabel('Pulse length (us)')
plt.xlabel('$\omega$ (rad/s)')
# Motor 2
plt.subplot(2, 2, 2)
plt.plot(M2['w'], M2['LENGTH'], '-*g', label='Motor 2 measured')
plt.plot(M2['w'], w2LENGTH(M2['w'], M2['ka'], M2['offset']), 'b', label='Motor 2 fitted')
plt.grid(True)
plt.legend(loc='best')
plt.ylabel('Pulse length (us)')
plt.xlabel('$\omega$ (rad/s)')
# Motor 3
plt.subplot(2, 2, 3)
plt.plot(M3['w'], M3['LENGTH'], '-*g', label='Motor 3 measured')
plt.plot(M3['w'], w2LENGTH(M3['w'], M3['ka'], M3['offset']), 'b', label='Motor 3 fitted')
plt.grid(True)
plt.legend(loc='best')
plt.ylabel('Pulse length (us)')
plt.xlabel('$\omega$ (rad/s)')
# Motor 4
plt.subplot(2, 2, 4)
plt.plot(M4['w'], M4['LENGTH'], '-*g', label='Motor 4 measured')
plt.plot(M4['w'], w2LENGTH(M4['w'], M4['ka'], M4['offset']), 'b', label='Motor 4 fitted')
plt.grid(True)
plt.ylabel('Pulse length (us)')
plt.xlabel('$\omega$ (rad/s)')
plt.show()





