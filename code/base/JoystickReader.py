# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 17:38:41 2016

@author: Grasiela Peccini
"""

import pygame
import time
done = False
pygame.init()

 
# Initialize the joysticks
pygame.joystick.init()
 

 
# -------- Main Program Loop -----------

while not done:
    # EVENT PROCESSING STEP
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
 
        # Possible joystick actions: JOYAXISMOTION JOYBALLMOTION JOYBUTTONDOWN
        # JOYBUTTONUP JOYHATMOTION
        if event.type == pygame.JOYBUTTONDOWN:
            print("Joystick button pressed.")
        if event.type == pygame.JOYBUTTONUP:
            print("Joystick button released.")
 
     
    # Get count of joysticks
    joystick_count = pygame.joystick.get_count()
 
       # For each joystick:
    for i in range(joystick_count):
        joystick = pygame.joystick.Joystick(i)
        joystick.init()
 
             
 
        # Get the name from the OS for the controller/joystick
        name = joystick.get_name()
        
 
        # Usually axis run in pairs, up/down for one, and left/right for
        # the other.
        axes = joystick.get_numaxes()
        
        
 
        for i in range(axes):
            axis = joystick.get_axis(i)
            print("Axis {} value: {:>6.3f}".format(i, axis))
        
 
        buttons = joystick.get_numbuttons()
        
        
        for i in range(buttons):
            button = joystick.get_button(i)
                   
 
        # Hat switch. All or nothing for direction, not like joysticks.
        # Value comes back in an array.
        hats = joystick.get_numhats()
               
 
        for i in range(hats):
            hat = joystick.get_hat(i)
        
    time.sleep(0.05)
 

pygame.quit()
