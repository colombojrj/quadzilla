# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 17:38:41 2016

Authors:
 - Grasiela Peccini
 - José Roberto Colombo Junior <colombojrj[AT]gmail.com>
 - Pedro Augusto Queiroz de Assis <pedroaugusto.feis[AT]gmail.com>
"""

import pygame
import time
import numpy as np
import serial

# Parameter configuration
SERIAL_PORT = "/dev/ttyUSB0"
BAUDRATE = 115200
Ts = 10e-3  # sampling time
Nwait = 2

# Initialize pygame, joystick and serial port
pygame.init()
pygame.joystick.init()
try:
    ser = serial.Serial(SERIAL_PORT, BAUDRATE, timeout=0.1)
    #ser.open()
except:
    ser = serial.Serial()

# Start joystick
joystick = pygame.joystick.Joystick(0)
joystick.init()
axes = joystick.get_numaxes()

# Auxiliary variable
axis_read = np.zeros(axes)

while(True):
  # Start counting time
  t_start = time.time()
  
  # Read the joystick
  pygame.event.get()

  # Draw the axis readings
  for i in range(axes):
    axis_read[i] = joystick.get_axis(i)

  # For Logitech Extreme 3D PRO
#  droll_ref  = int(axis_read[0] * 127)
#  dpitch_ref = int(axis_read[1] * 127)
#  dyaw_ref   = int(axis_read[2] * 127)
#  throttle = int(2**7 * (1 -axis_read[3]) -1) # 2**8 * (1 -axis_read[3])/2 -1
#  if throttle <= 0:
#      throttle = 0

  # For Davi's RC
  droll_ref  = int(axis_read[0] * 127 * 2)
  dpitch_ref = int(axis_read[1] * 127 * 2)
  dyaw_ref   = int(axis_read[5] * 127 * 2)
  throttle   = int(2**8 * (0.7 -axis_read[2]))
  throttle = min(255, throttle)
  if throttle <= 0:
      throttle = 0
  droll_ref = min(127, droll_ref)
  if droll_ref < -127:
    droll_ref = -127
  dpitch_ref = min(127, dpitch_ref)
  if dpitch_ref < -127:
    dpitch_ref = -127
  dyaw_ref = min(127, dyaw_ref)
  if dyaw_ref < -127:
    dyaw_ref = -127

  # Buttons
  #
  # Button
  #    0       1       2       3       4       5      6       7       8       9      10      11    
  # nothing nothing nothing nothing nothing nothing start nothing nothing nothing nothing abort 
  b = 0
  for k in range(0, 12):
    b = b | (joystick.get_button(k) << k+1)
  b = int(b)

  # Create output vector
  output = [chr(dpitch_ref) if dpitch_ref>0 else chr(0x80 | abs(dpitch_ref)),
            chr(droll_ref) if droll_ref>0 else chr(0x80 | abs(droll_ref)),
            chr(dyaw_ref) if dyaw_ref>0 else chr(0x80 | abs(dyaw_ref)),
            chr(0xFF & throttle),
            chr((0xFF00 & b) >> 8), chr(0xFF & b)]
            
  # Create checksum
  checksum = 0
  for item in output: checksum = checksum + ord(item)
  output.append(chr(checksum & 0xFF))

  # Send data
  for item in output: ser.write(item)
  ser.flush()
  
  # Wait for next sampling period
  elapsed_time = time.time() -t_start
  time_to_wait = 2*Ts -elapsed_time
  if time_to_wait > 0:
    time.sleep(time_to_wait)
  
  pitchp = round(dpitch_ref, 4)
  rollp = round(droll_ref, 4)
  yawp = round(dyaw_ref, 4)
  w0p = round(100.*throttle/255., 4)
  print "Pitch: %d, Roll: %d, Yaw: %d, w0: %d%%" %(pitchp, rollp, yawp, w0p)
  #print 'Pitch:', pitchp, 'Roll:', rollp, 'Yaw:', yawp,'w0:', w0p
  
    
# Disable pygame and close the serial port
pygame.quit()
ser.close()
