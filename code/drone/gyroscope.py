#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to work with the sensor module GY-85 which contains :
 -> (developing)ITG-3200, from InvenSense;
 -> (Not yet) ADXL345, from Analog Devices;
 -> (Not yet) HMC5883L, from Honeywell.
It is assumed that the sensor is connected via I2C

Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 30/10/2014

"""

# Loads the necessary libraries
#import smbus
from numpy import array, append
#from numpy import append
import time

class ITG_3200:
  """
  Class: ITG-3200
  File: gyroscope.py
  Purpose: Python library to work with ITG-3200 on GY-85.
  Created on: 30/10/2014
  """

  def __init__(self, bus, CLOCK="PLL_X", FILTER="LOW_10Hz", FSAMPLE=0, TIMEOUT=1, debug=False):
    """
    ITG-3200 constructor
	
    __init__(bus, CLOCK=, FILTER, FSAMPLE, debug=False)

    Parameters
    ----------
    bus : I2C bus object (where the sensor is connected to)
    CLOCK SOURCE: string specifying the clock source of the device (default: "PLL with X Gyro reference")
    FILTER : string specifying which filter configuration must be used (default: "LOW_10Hz")
    FSAMPLE : int (in HZ) specifying which is the sample rate desired (default according to the filter selected)
  
    """

    # Keep bus information as internal
    self.bus = bus

    # Address ITG-3200
    self.ADDRESS = 0x68

    # Check communication and identity of ITG-3200
    self.CHIP_ID = bus.read_word_data(self.ADDRESS, 0) & 0b1111110
    if (self.CHIP_ID != self.ADDRESS):
      print "*******************************************************"
      print "** Problem with gyroscope! Please, check the sensor! **"
      print "*******************************************************"
    else: # If it's working fine, then start setting the ITG-3200
      if debug:
        print "(OK) I found the gyroscope ITG_3200"
      # Clock Selection
      CLOCK_CODE = {"INTERNAL"       : 0b000,
                    "PLL_X"          : 0b001,
                    "PLL_Y"          : 0b010,
                    "PLL_Z"          : 0b011,
                    "PLL_32.768KHz"  : 0b100,
                    "PLL_19.2MHz"    : 0b101}

      CLOCK_NAME = {"0" : "INTERNAL",
                    "1" : "PLL_X",
                    "2" : "PLL_Y",
                    "3" : "PLL_Z",
                    "4" : "PLL_32.768 kHz",
                    "5" : "PLL_19.2 MHz"}

      # Possible configuration to internal filter
      FILTER_CODE = {"LOW_5Hz"       : 0b110 ,
                     "LOW_10Hz"      : 0b101 ,
                     "LOW_20Hz"      : 0b100 ,
                     "LOW_42Hz"      : 0b011 ,
                     "LOW_98Hz"      : 0b010 ,
                     "LOW_188Hz"     : 0b001 ,
                     "LOW_256Hz"     : 0b000 ,}

      FILTER_NAME = {"0" : "Low-pass, f_cut=256Hz",
                     "1" : "Low-pass, f_cut=188Hz",
                     "2" : "Low-pass, f_cut=98Hz" ,
                     "3" : "Low-pass, f_cut=42Hz" ,
                     "4" : "Low-pass, f_cut=20Hz" ,
                     "5" : "Low-pass, f_cut=10Hz" ,
                     "6" : "Low-pass, f_cut=5Hz" ,}

      # Translate the selected configuration
      self.CLOCK_SELECTED  = CLOCK_CODE[CLOCK]
      self.FILTER_SELECTED = FILTER_CODE[FILTER]
########################################################
      #Setting the Low Pass Filter, and in consequence FINTERNAL, adopted
      FILTER_DATA = bus.read_word_data(self.ADDRESS, 0x16) & 0b11111111
      if ((FILTER_DATA & 0b00011111) != (0b11000 | self.FILTER_SELECTED)):
        FILTER_DATA = FILTER_DATA & 0b11100000
        bus.write_byte_data(self.ADDRESS, 0x16, (FILTER_DATA | 0b11000 | self.FILTER_SELECTED))
        if debug:
        # Checks the filter set, and the value FSEL
          FILTER_DATA = (bus.read_word_data(self.ADDRESS, 0x16) & 0b111)
          print "(**) Filter is set to:", FILTER_NAME[str(FILTER_DATA)]
          FSEL = (bus.read_word_data(self.ADDRESS, 0x16) & 0b11000) >> 3
          print "(**) FSEL must be setted to 3, and now it's value is: ",FSEL 
########################################################
      #Setting the divider to work with the sample rate choosen
      if (FILTER_CODE==0):
        FINTERNAL=8000.
      else:
        FINTERNAL=1000.
      if (FSAMPLE==0):
        FSAMPLE=FINTERNAL
      SMPLRT_DIV=int(FINTERNAL/FSAMPLE-1)
      if (SMPLRT_DIV<0):
        SMPLRT_DIV=0
        print "Sample rate too high, choosing the maximum available"
      elif (SMPLRT_DIV>255):
        SMPLRT_DIV=255
        print "Sample rate too low, choosing the minimum available"
      bus.write_byte_data(self.ADDRESS, 0x15, SMPLRT_DIV) # & 0b11111111
      if debug:
        # Checks the fsample selected
        SMPLRT_DIV = (bus.read_word_data(self.ADDRESS, 0x15) & 0b11111111)
        FSAMPLE = FINTERNAL/(SMPLRT_DIV+1)
        print "(**) The sample rate is: ",FSAMPLE
 ########################################################
      # Setting the interrupt configuration
      # Configuration: Active level High, Open Drain, Latch until interrupt is cleared, Status register Read,
      # enable device ready, Enable data interrupt (pag 26)
      bus.write_byte_data(self.ADDRESS, 0x17,0b01100101)
      if debug:
        # Checks the filter set
        INT_CONFIG = (bus.read_word_data(self.ADDRESS, 0x17) & 0b11111111)
        print "Enable interrupt when data is available, this is ",bool(INT_CONFIG & 00000001)
        print "Enable interrupt when device is ready, this is ",bool((INT_CONFIG & 00000100)>>2)
        print "Clear method when register Status is read, this is ",bool(not((INT_CONFIG & 00010000)>>4))
        print "Interrupt mode is selected to be latched, this is ",bool((INT_CONFIG & 00100000)>>5)
        print "The output pin is in open-drain configuration, this is ",bool((INT_CONFIG & 01000000)>>6)
        print "Logic level of output pin is setted to high, this is ",bool(not((INT_CONFIG & 10000000)>>7))
########################################################
      #Selecting the the Clock Source
      CLOCK_DATA = bus.read_word_data(self.ADDRESS, 0x3E) & 0b11111111
      if ((CLOCK_DATA & 0b111) != self.CLOCK_SELECTED):
        CLOCK_DATA = CLOCK_DATA & 0b11111000
        bus.write_byte_data(self.ADDRESS, 0x3E, (CLOCK_DATA | self.CLOCK_SELECTED))
        #waiting PLL ready after changing clock source
        START_CHANGE=time.time() # time.time() returns a measured time in SECONDS.
        INT_STATUS = (bus.read_byte_data(self.ADDRESS, 0x1A) & 0b100) >> 2
        DUMMY=0
        while (INT_STATUS==0): #i think there is a better way to do this, but i dont know how (maybe a function that i dont know yet) 
          if (DUMMY==0):
            if debug:
              print "Please wait changing the clock source."
            DUMMY=1
          time.sleep(1e-3) #1[ms]	
          INT_STATUS = ((bus.read_word_data(self.ADDRESS, 0x1A) & 0b100)>>2)
          END_CHANGE=time.time() 
          if ((END_CHANGE - START_CHANGE) > TIMEOUT):
            INT_STATUS=1
            print "CAUTION! There is something wrong during the clock changing. Please,check that."
      if debug:
        # Checks the Clock Source Setted
        CLOCK_DATA = bus.read_word_data(self.ADDRESS, 0x3E) & 0b111
        #print "(**) Clock source is :", CLOCK_CODE[str(CLOCK_DATA)] # WRONG ! You must provide key , NOT the value to the Dictionary!
        print "(**) Clock source is:", CLOCK_NAME[str(CLOCK_DATA)]

########################################################
  #Reset the sensor function
  def factoryreset(self):
    """
    ITG-3200 factory reset
    This function resets the device and internal registers to the power-up-default settings.
    After reset the device, the object must be constructed again.
    """
    IS_VALID=0
    while not(IS_VALID):
      SURE=str(raw_input('THIS WILL RESET THE DEVICE AND ALL INTERNAL REGISTERS TO POWER-UP-DEFAULT! ARE YOU SURE?(Y/N)'))
      if (int(SURE=="y") | int(SURE=="n") | int(SURE=="Y") | int(SURE=="N")):
        IS_VALID=1
      else:
        print "This is not a valid input, please try again."
      if (int(SURE=="y") | int(SURE=="Y")):
        self.bus.write_byte_data(self.ADDRESS, 0x3E, 0b10000000)
#####################################################################
  def __extract_data(self,MSB, LSB):
    """
    private function to extract data from msb and lsb numbers from sensor
    details at datasheet, page 27
    """
    GYRO = (MSB << 8 | LSB)
    SGN = 1 - ( (MSB & 0x80) >> 6)
    if (SGN > 0):
      return (GYRO & 0x7FFF)
    else:
      return ( (0x7FFF - (GYRO & 0x7FFF)) * SGN )

  def read(self, return_raw_data=False, debug=False):
    """
    gyro = read(return_raw_data=False, debug=False)
    It reads the Angular Rate on X-, Y- and Z-Axis
    Parameters
    -----------
    return_raw_data : return raw data or angular data? (default: False)
    debug : debug flag (default: False)
    Returns
    --------
    gyro = array([gyro_x, gyro_y, gyro_z])
    """
    data = self.bus.read_i2c_block_data(self.ADDRESS, 0x1d,6)
    
###########Extract Angular data from raw data
    GYRO_X = self.__extract_data(data[0],data[1])
    GYRO_Y = self.__extract_data(data[2],data[3])
    GYRO_Z = self.__extract_data(data[4],data[5])	
    if debug:
      print GYRO_X,GYRO_Y,GYRO_Z
########Also, printing the temperature
      
    if return_raw_data:
      return array([GYRO_X,GYRO_Y,GYRO_Z])
    else:
      # Dividing the readings by 14.375 will return the º/s readings
      # Since the model is in rad/s: (pi/180)*readings/14.375 = readings*0.0012
      return array([GYRO_X, GYRO_Y, GYRO_Z])*0.0012141420883438813

def main():
  import smbus
  import numpy as np
  bus = smbus.SMBus(1)
  gyro = ITG_3200(bus)
  data = gyro.read()
  print 'wx, wy, wz:' + str(data[0]) + ', ' + str(data[1]) + ', ' + str(data[2])
  bus.close()
  
  return 0

if __name__ == '__main__':
        main()
