#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to work with compass:
 -> HCM5883, from  Honeywell

It is assumed that the sensor is connected via I2C

Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 29/01/2015

"""

# Loading the necessary libraries
#import smbus
from numpy import array, pi, arctan2, sin, cos
import time

def yaw(readings):
  """
  It returns the heading angle of the quad in relation of magnetic field

  Parameters
  -----------
  None

  Returns
  --------
  yaw in radians
  """

  return -arctan2(readings[1], readings[0])

class HCM5883:
  """
  Class: HCM5883
  File: compass.py
  Purpose: Python library to work with HCM5883, from Honeywell
  Created on: 29/01/2015
  """

  def __init__(self, bus, NUMBER_SAMPLES="1", OUTPUT_RATE="15", DEVICE_GAIN="0.92", OP_MODE="CONTINUOS", offset_x=0., offset_y=0., debug=False):
    """
    HCM5883 constructor

    __init__(bus, OPERATION_MODE, RANGE, FILTER, debug=False)

    Parameters
    ----------
    bus : I2C bus object (where the sensor is connected to)
    NUMBER_SAMPLES : string specifying the numer of samples per measurement output (default:"1")
    OUTPUT_RATE    : string specifying the output rate in Hz (default: "15")
    DEVICE_GAIN    : string specifying the device gain in LSB/gauss (default: "0.92")
    OP_MODE        : string specifying the operation mode of the device (default: "CONTINUOS")
    """

    # Keep bus information as internal variable
    self.bus = bus

    # Some useful HCM5883 definitions
    self.ADDRESS = 0x1e
    self.IDENTITY_A = 0b01001000
    self.IDENTITY_B = 0b00110100
    self.IDENTITY_C = 0b00110011
    # Output Rate (Hz), CRA
    OUTPUT_RATE_CONFIG = {"0.75": 0b000 ,
                          "1.5" : 0b001 ,
                          "3"   : 0b010 ,
                          "7.5" : 0b011 ,
                          "15"  : 0b100 ,
                          "30"  : 0b101 ,
                          "75"  : 0b110 }

    # Number of samples averaged per measurement, CRA
    NUMBER_SAMPLES_CONFIG = {"1" : 0b00 ,
                             "2" : 0b01 ,
                             "4" : 0b10 ,
                             "8" : 0b11 }

    # Device Gain, CRB (LSB/Gauss)
    DEVICE_GAIN_CONFIG = {"0.73" : 0b000 ,
                          "0.92" : 0b001 ,
                          "1.22" : 0b010 ,
                          "1.52" : 0b011 ,
                          "2.27" : 0b100 ,
                          "2.56" : 0b101 ,
                          "3.03" : 0b110 ,
                          "4.35" : 0b111 }
    #Operation mode of the device
    OP_MODE_CONFIG =    {"CONTINUOS" : 0b00  ,
                         "SINGLE"    : 0b01  ,
                         "IDLE"      : 0b11  }
    
    # Translate the selected configuration
    self.DEVICE_GAIN_VALUE = float(DEVICE_GAIN)
    self.OUTPUT_RATE       = OUTPUT_RATE_CONFIG[OUTPUT_RATE]
    self.NUMBER_SAMPLES    = NUMBER_SAMPLES_CONFIG[NUMBER_SAMPLES]
    self.DEVICE_GAIN       = DEVICE_GAIN_CONFIG[DEVICE_GAIN]
    self.OP_MODE           = OP_MODE_CONFIG[OP_MODE]

    # Check if HCM5883 is properly working
    self.CHIP_ID_A = bus.read_byte_data(self.ADDRESS, 10)
    self.CHIP_ID_B = bus.read_byte_data(self.ADDRESS, 11)
    self.CHIP_ID_C = bus.read_byte_data(self.ADDRESS, 12)
    if (self.CHIP_ID_A != self.IDENTITY_A) and (self.CHIP_ID_B != self.IDENTITY_B) and (self.CHIP_ID_C != self.IDENTITY_C):
      print "**************************************************************"
      print "**    Problem with compass! Please, abort the operation!    **"
      print "**************************************************************"
    else: # If it's working fine, then start setting the HMC5883L
      if debug:
        print "(OK) I found the compass HMC5883L"
     
      # Operation mode configuration
      # Configuration register A: OUTPUT_RATE, NUMBER_SAMPLES, MEASUREMENT_MODE(Normal)
      CRA= (0b00000000)|(self.NUMBER_SAMPLES << 5)|(self.OUTPUT_RATE << 2)
      bus.write_byte_data(self.ADDRESS, 0x00, CRA)
      
      if debug:
        CRB_DATA=bus.read_byte_data(self.ADDRESS, 0x00)
        print "(**) The byte in Configuration Register A is", bin(CRB_DATA)

      # Configuration register B: DEVICE_GAIN
      CRB= (0b00000000)|(self.DEVICE_GAIN << 5)
      bus.write_byte_data(self.ADDRESS, 0x01, CRB)

      if debug:
        # Checks the range set
        CRB_DATA = bus.read_byte_data(self.ADDRESS, 0x01)
        print "(**) The byte in Configuration Register B is", bin(CRB_DATA)

      # Configuration Mode Register: OP_MODE
      MR= (self.OP_MODE)
      bus.write_byte_data(self.ADDRESS, 0x02, MR)
      
      # Set the offset (from callibration)
      self.offset_x = offset_x
      self.offset_y = offset_y
      
      # Calibrate the compass to remove the bias (i.e., to point to zero)
      self.calibrate()

      if debug:
        # Checks the range set
        MR_DATA = bus.read_byte_data(self.ADDRESS, 0x02)
        print "(**) The byte in Configuration Mode register is", bin(MR_DATA)
            
##############################

  def __extract_data(self, MSB, LSB):
    """
    private function to extract data from msb and lsb numbers from sensor
    details at datasheet, page 27
    """
    # remove non-used bits
    MSB = MSB & 0x0F

    MEASURE = ((MSB << 8) | LSB)
    SGN = 1 - ( (MSB & 0b1000) >> 2)
    if (SGN > 0):
      return (MEASURE & 0x7FF)
    else:
      return ( (0x7FF - (MEASURE & 0x7FF)) * SGN )

##############################

  def read(self, return_raw_data=False, debug=False):
    """
    magneto = read(return_raw_data=False, debug=False)
    It reads the Magnetic Field Vector on X-, Y- and Z-Axis
    Parameters
    -----------
    debug : debug flag (default: False)
    Returns
    --------
    magneto = array([x, y, z]) [mG]
    """
                                        
    data = self.bus.read_i2c_block_data(self.ADDRESS, 3,6)
    # Extract Angular data from raw data
    if return_raw_data:
      mag_x = array([data[0],data[1]])
      mag_z = array([data[2],data[3]])
      mag_y = array([data[4],data[5]])
    else:
      mag_x = self.__extract_data(data[0],data[1])*self.DEVICE_GAIN_VALUE
      mag_z = self.__extract_data(data[2],data[3])*self.DEVICE_GAIN_VALUE
      mag_y = self.__extract_data(data[4],data[5])*self.DEVICE_GAIN_VALUE
      
      # Remove the offset (i.e., center the circle of readings to origin)
      x = mag_x -self.offset_x
      y = mag_y -self.offset_y
      
      # Remove the initial angle bias
      mag_x =  x*self.bias_cos +y*self.bias_sin
      mag_y = -x*self.bias_sin +y*self.bias_cos
    
    if debug:
      # The data will be printed
      print mag_x, mag_y, mag_z
    
    return array([mag_x, mag_y, mag_z])
##############################

  def calibrate(self):
    """
    It sets the angles as ZERO in this initial position
    """
    
    # Reads the compass
    data = self.bus.read_i2c_block_data(self.ADDRESS, 3,6)
    mag_x = self.__extract_data(data[0],data[1])*self.DEVICE_GAIN_VALUE
    mag_y = self.__extract_data(data[4],data[5])*self.DEVICE_GAIN_VALUE
    
    # Remove the offset (i.e., center the circle of readings to origin)
    mag_x = mag_x -self.offset_x
    mag_y = mag_y -self.offset_y
    
    # Calculate the bias parameters
    self.bias = arctan2(mag_y, mag_x)
    self.bias_sin = sin(self.bias)
    self.bias_cos = cos(self.bias)

def main():
  import smbus
  import numpy as np
  bus = smbus.SMBus(1)
  compass = HCM5883(bus,offset_x = 166,offset_y = 165)
  data = compass.read()
  print 'x,y:' + str(data[0]) + ', ' + str(data[1])
  print 'Yaw angle: ', (180./np.pi)*yaw(data)
  print 'Bias:', compass.bias*180/np.pi
  bus.close()
  
  return 0

if __name__ == '__main__':
        main()
