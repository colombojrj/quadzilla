#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to compute the Kalman Filter



Authors: 
  Luiz Arthur Gagg Filho <luizarthur.gagg[at]gmail.com>222
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 20/01/2016

"""

# Loading the necessary libraries
import numpy as np   
def Kalman_design(Ad, C, Pw, Pv, P0 = [], kend = 200):
  """
  This function calculates the Kalman of a estimator. This gain is to be used in the following observer loop sequence:
  
  step 1 (correction): x(k|k) = x(k|k-1) + K_kalman (y(k) - C x(k|k-1))
  step 2 calculate the control 
  step 3 (propagation): x(k+1) = A x(k|k) + B * u(k)
  
  
  Input parameters:
   Discrete state space matrices: Ad, Bd
   Standard deviation vectors: sd_w (state noise), sd_v (output noise)*
   Number of iterations of gain calculation: kend (default is 200)
   Initial covariance: P0 (defult is equal sd_w.T*sd_w)
   
  Output variables:
   Kalman gain: K_kalman
  
  """
  #sd_w and sd_z are the vectors with the standard deviation of each state or output  
  n = np.size(Ad,0)  
  if P0 == []:
    P = np.matrix(np.eye(n,n)) 
  else:
    P = P0
  for k in xrange(0, kend):
    P = Ad*P*Ad.T + Pw
        
    K_kalman =  P*C.T*np.linalg.inv((C*P*C.T + Pv))
    P = (np.matrix(np.eye(n)) - K_kalman*C)*P
    #these equations come from my Elderś Stochastic Discipline
      
  return K_kalman

def estimate_propform(Ad, Bd, C, D, Kobs, xhat, u, ym):
  """
  The purpose of this function is to estimate the states using the propagation form. This means, this function will calculate the estimation on k+1 with the informations of the instant k.
  
  Note that, to the estimatives converge to the correct value the matrix (A-Kobs*C) must have eigenvalues inside the unitary disk.
  
  This function works with direct feedthrough.
  
  Input parameters:
   Discrete state space matrices: Ad, Bd, C, D
   Observer gain: Kobs
   Estimated States in the previous step (k-1): xhat(k|k-1)
   Control of the instant (k): u(k)
   Outputs measured in the instant k: ym(k)
   
  Output variables:
   Estimated States in the next instant (k+1): xhat(k+1|k)
  
  """
    
  #yhat(k) = C * xhat(k|k-1) + D *u(k)
  yhat = C*xhat + D*u
    
  #propagation: xhat(k+1|k) = A xhat(k|k-1) + B u(k) + L*(y(k)-yhat(k))
  xhat = Ad*xhat + Bd*u + Kobs*(ym - yhat)

  return xhat
  
def estimate_corrform(Ad, Bd, C, Kobsl, xhat, u, ym):
  """
  The purpose of this function is to estimate the states using the correction form. This means, this function will use the last information of the sensors to correct the estimated states of the last instant (xhat(k|k-1)).
  
  Note that, to the estimatives converge to the correct value the matrix (A-Kobs*C) must have eigenvalues inside the unitary disk.
  
  Remember Kobs = A*Kobsl, the function uses Kobsl!
  
  This function do not works with direct feedthrough.
  
  Input parameters:
   Discrete state space matrices: Ad, Bd, C, D
   Observer gain: Kobs
   Estimated States in the previous step (k-1): xhat(k|k-1)
   Control of the previous instant (k-1): u(k-1)
   Outputs measured in the instant k-1: ym(k-1)
   
  Output variables:
   Estimated States in the actual instant (k): xhat(k|k)
  
  """
  xhat = estimate_propagate(Ad, Bd, xhat, u)

  # yhat(k|k-1) = C * xhat(k|k-1)
  yhat = C*xhat
  # correction: xhat(k\k) = xhat(k|k-1) + Kobs' * (ym(k) - yhat(k|k-1))
  #print yhat
  xhat = xhat + Kobsl*(ym - yhat)
  return xhat
  
def estimate_propagate(Ad, Bd, xhat, u):
  """
  The purpose of this function is to propagte the estimatives based on the input of the instant k.
  
  
  Input parameters:
   Discrete state space matrices: Ad, Bd
   Estimated States in the instant k: xhat(k|k)
   Control of the actual instant k: u(k)
      
  Output variables:
   Propagation of the estimatives to the next instant k+1: xhat(k+1|k)
  
  """  
  #propagation: xhat(k+1|k) = A xhat(k|k) + B u(k)
  xhat = Ad*xhat + Bd*u

  return xhat

def extended_matrices(Ad, Bd, Cd):
  """
  The purpose of this function is to generate the extented matrices to do the disturbance estimation.
  It is assume constant input disturbance. The output can be used to correct the input.
  
  I have inserted a function to return the extended state space matrices. These matrices \
  can be used to estimate overall input disturbances. This way the last four states \
  estimated are the equivalent disturbances in each input. These estimated disturbances \
  can be used as input of the system to compensate model mismatch  
  
  Input parameters:
   Discrete state space matrices: Ad, Bd, Cd
         
  Output variables:
   Extended matrices Ade, Bde, Cde
  
  """  
  nx = np.size(Ad,0) 
  nu = np.size(Bd,1)
  ny = np.size(Cd,0)
  
  
  Ade = np.vstack(( np.hstack((Ad, Bd)), np.hstack((np.matrix(np.zeros((nu, nx))), np.matrix(np.eye(nu))) )  ))
  Bde = np.vstack( (Bd, np.matrix(np.zeros((nu,nu))) ) )
  Cde = np.hstack( (Cd, np.matrix(np.zeros((ny,nu))) ) )
  return Ade, Bde, Cde

class EKF:
  """
  Class: EKF
  File: state_observer.py
  Purpose: Python implementation of the Extended Kalman Filter
  Created on: 27/08/2016
  """

  def __init__(self, Pw, Pv):
    '''
    Inputs:
      Pw - state noise matrix (numpy matrix type)
      Pv - output noise matrix (numpy matrix type)
    '''
    
    self.nx = Pw.shape[0]
    self.nu = Pw.shape[0]
    