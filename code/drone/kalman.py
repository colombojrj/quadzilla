#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to compute the Kalman Filter



Authors: 
  Luiz Arthur Gagg Filho <luizarthur.gagg[at]gmail.com>222
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 23/08/2015

"""

# Loading the necessary libraries
import time
from numpy import *

class Kalman:
  def __init__(self):
    # Loading the system matrix
    # Foi assumido que o modelo já está carregado com variáveis globais
    # sys = Ad, Bd, Cd, Dd
    #
	self.n = size(Ad,0)
	 
	
    #condições iniciais
    self.P = self.Sw 
    self.xhat = mat(zeros((self.n,1)))
  
  def noisevectors(self,sd_w,sd_z):
    #sd_w and sd_z must be matrices
    self.Sw = sd_w*transpose(sd_w);
    self.Sz = sd_z*transpose(sd_z);       
          
  def estimate_x(self):            
   # Calcula o ganho de Kalman
    K = Ad*self.P*tranpose(Cd)*linalg.inv*(Cd*self.P*transpose(Cd) + self.Sz)
    
    # estimativa do próximo estado (xhat(k+1))
    self.xhat = Ad*self.xhat + Bd*u + K*(y - Cd*self.xhat)
    
    # Atualização matriz covariância    
    self.P = Ad*self.P*transpose(Ad)+self.Sw - Ad*self.P*transpose(Cd)*linalg.inv(self.Sz)*Cd*self.P*transpose(Ad)
    
