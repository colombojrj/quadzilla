#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to implement an onboard controller
-> support to PID

Authors:
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 25/06/2016

"""

# Loading the necessary libraries
from numpy import inf, zeros
from scipy.signal import butter

class second_order_filter:
  """
  Class: 2nd_order_filter
  File: controller.py
  Purpose: Python implementation of a 2nd order discrete time filter 
  Created on: 16/07/2016
  """
  def __init__(self, num, den):
    """
    2nd_order_filter constructor 
    
    Parameters:
    -----------
    num: array with numerator of transfer function (see scipy.signal.butter)
    
    den: array with denominator of transfer function (see scipy.signal.butter)
    """
    
    # Filter internal variables (u[k], u[k-1], ...)
    self.__u  = 0.
    self.__u1 = 0.
    self.__u2 = 0.
    self.__y  = 0.
    self.__y1 = 0.
    self.__y2 = 0.
    
    # Store num and den
    self.__num = num
    self.__den = den
  
  def __HouseKeeping(self):
    self.__u2 = self.__u1
    self.__u1 = self.__u
    self.__y2 = self.__y1
    self.__y1 = self.__y
    
  
  def UpdateFilter(self, u):
    self.__HouseKeeping()
    self.__u = u    
    self.__y = ( u*self.__num[0] +self.__u1*self.__num[1] + \
                 self.__u2*self.__num[2] -self.__y1*self.__den[1] + \
                -self.__y2*self.__den[2])
    self.__y = self.__y / self.__den[0]
    return self.__y
  
  def get_y(self):
    return self.__y

class n_order_filter:
  """
  Class: n_order_filter
  File: controller.py
  Purpose: Python implementation of a n order discrete time filter 
  Created on: 16/07/2016
  """
  def __init__(self, num, den):
    """
    n_order_filter constructor 
    
    Parameters:
    -----------
    num: array with numerator of transfer function (see scipy.signal.butter)
    
    den: array with denominator of transfer function (see scipy.signal.butter)
    """
    
    # Filter internal variables (u[k], u[k-1], ...)
    self.__n = num.shape[0]
    self.__u = zeros(self.__n)
    self.__y = zeros(self.__n)
    
    # Store num and den
    self.__num = num
    self.__den = den
  
  def __HouseKeeping(self):
    self.__u[1:] = self.__u[0:-1]
    self.__y[1:] = self.__y[0:-1]
  
  def UpdateFilter(self, u):
    self.__HouseKeeping()
    self.__u[0] = u
    y = sum(self.__u*self.__num) -sum(self.__y[1:]*self.__den[1:])
    self.__y[0] = y / self.__den[0]
    return self.__y[0]
  
  def get_y(self):
    return self.__y[0]

class PID:
  """
  Class: PID
  File: controller.py
  Purpose: Python implementation of a PID controller
  Created on: 25/06/2016
  """

  def __init__(self, Kp, Ki, Kd, Ts, 
               # optinal arguments
               umin=-inf, umax=inf, emin=-inf, emax=inf,
               wn=0.):
    """
    PID constructor

    __init__(Kp, Ki, Kd, Ts, umin=-inf, umax=inf, emin=-inf, emax=inf, wn=0.}
    
    Parameters
    ----------
    Kp : proportional gain
    
    Ki : integral gain

    Kd : derivative gain

    Ts : sampling time
    
    Optional parameters:
    --------------------
    umin : lower bound of control signal (assumes -inf)
    
    umax : upper bound of control signal (assumes inf)
    
    emin : lower bound of error signal (assumes -inf)
    
    emax : upper bound of error signal (assumes inf)
    
    wn   : cut frequency of filter in derivative channel
    """

    # Specified low pass filter?
    if wn == 0.:
      self.filter = False
    else: 
      self.filter = True

    # Internal variables
    self.u = 0. # initialize control variable
    self.__ui = 0.
    self.__e0 = 0.
    self.__e1 = 0.
    self.__e2 = 0.
    if self.filter:
      self.wn = wn
    self.Kp = Kp
    self.Ki = Ki
    self.Kd = Kd
    self.Ts = Ts
    self.umax = umax
    self.umin = umin
    self.emax = emax
    self.emin = emin
    
    # Design the low pass filter
    self.__b, self.__a = butter(2, wn, btype='low', analog=False)
    self.__f = n_order_filter(self.__b, self.__a)
  
  def __HouseKeeping(self):
    """
    Function to manage the internal variables of the PID
    """
    self.__e2 = self.__e1
    self.__e1 = self.__e0
  
  def __saturate_e(self):
    """
    Function to saturate the input
    """
    if self.__e0 > self.emax:
      self.__e0 = self.emax
    if self.__e0 < self.emin:
      self.__e0 = self.emin
  
  def __saturate_u(self):
    """
    Function to saturate the control action
    """
    if self.u > self.umax:
      self.u = self.umax
    if self.u < self.umin:
      self.u = self.umin
        
  def calculate_control(self, yk, ref=0.):
    """
    This function updates the control and manage the internal PID sequence
    of actions:
    - HouseKeeping, update the state of the PID
    - Update e[0] with given information
    - If specified, it saturates the input and output
    - Calculate the control action
    
    Inputs:
    - yk: plant output
    - ref: reference signal (it assumes 0.0)
    
    Output:
    - u: control action
    """
    self.__HouseKeeping()
    self.__e0 = ref - yk
    self.__saturate_e()
    
    # Proportional gain
    up = self.Kp * self.__e0
    
    # Integral gain
    self.__ui = self.__ui + self.Ki*self.Ts*(self.__e0 +self.__e1)/2.
    
    # Derivative gain
    ud = self.Kd * (self.__e0 -self.__e1) / self.Ts
    
    if self.filter:
      ud = self.__f.UpdateFilter(ud)
    
    self.u = up + self.__ui + ud
    self.__saturate_u()
    return self.u
  
  def get_PID_state(self):
    """
    This function returns the PID state, i.e., e[k], e[k-1] and e[k-2]
    If a 2nd order filter was used, then return also u[k], u[k-1] and u[k-2]
    """
    return self.__e0, self.__e1, self.__e2
  
  def get_2nd_order_filter(self):
    return self.__a, self.__b

# Test functions declaration
def test_pid():
  #pid = PID(100, 100, 0, 0.1, umax = 50.)
  pid = PID(100, 100, 100, 0.1, umin=-50., umax = 50., wn = 0)
  print pid.calculate_control(0.1)
  
def test_2nd_order_filter():
  from scipy.signal import butter
  from numpy.random import randn
  from numpy import sin, arange, zeros, pi
  import pylab as plt
  
# Design a filter  
  num, den = butter(2, 0.0)
  print num, den
  
  # Create a signal
  signal = sin(arange(0, 4*pi, 0.01))
  
  # Make it noisy
  signal = signal + randn(signal.shape[0])
  
  # Vector to store data
  signal_f = zeros(signal.shape[0])
  signal_fn= zeros(signal.shape[0])
  
  # Create filter object and filter data!
  f = second_order_filter(num, den)
  fn= n_order_filter(num, den)
  
  for k in range(0, signal.shape[0]):
    signal_f[k] = f.UpdateFilter(signal[k])
    signal_fn[k]= fn.UpdateFilter(signal[k])
  
  # Plot data
  plt.plot(signal)
  plt.plot(signal_f, 'r')
  plt.plot(signal_fn,'g')
  plt.grid(True)
  plt.show()
    
if __name__ == '__main__':
  test_pid()
  #test_2nd_order_filter()
    
    
    
    
    
    
    
    
    
    
    
