# -*- coding: utf-8 -*-

# Function to verify data integrity
def checksum_ok(data):
  checksum = 0
  for item in data[0:-1]: checksum = checksum + ord(item)
  if (checksum & 0xFF) == ord(data[-1]):
    return 1
  else:
    return 0

def decode(data, right_size):
  # dictionary with result of decoding
  r = dict() # result

  # If right size matches
  if len(data) != right_size:
    r['error'] = 1
  else:
    if checksum_ok(data):
      try:
        r['pitch'] = -(ord(data[0]) & 0x7F) if (ord(data[0]) & 0x80) else ord(data[0])
        r['roll'] = -(ord(data[1]) & 0x7F) if (ord(data[1]) & 0x80) else ord(data[1])
        r['yaw'] = -(ord(data[2]) & 0x7F) if (ord(data[2]) & 0x80) else ord(data[2])
        r['throttle'] = ord(data[3])
        buttons = (ord(data[4]) << 8) + ord(data[5])
        r['fire_up'] = 1 if (buttons & (1 << 7)) else 0
        r['abort'] = 1 if (buttons & (1 << 12)) else 0
        r['error'] = 0
      except:
        r['error'] = 1
    else:
      r['error'] = 1
  return r

def main():
  import serial, time
  ser = serial.Serial('/dev/ttyO5', 115200, timeout=1)
  ser.open()

  # Define right size
  right_size = 7
  
  while(1):
    #print ser.inWaiting()
    if ser.inWaiting() == right_size:
      data = ser.read(right_size)
      refs = decode(data, right_size)
      print 'pitch:', refs['pitch'], 'roll:', refs['roll'], 'yaw:', refs['yaw'],'w0:', refs['throttle']
    elif ser.inWaiting() > right_size:
      ser.flushInput()
    time.sleep(10e-3)

if __name__ == '__main__':
  main()
