#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to make the data fusion of navigation sensors.

It is assumed the angles
- P: pitch on body frame
- Q: roll on body frame
- R: yaw on body frame
- phi:   pitch as Euler angle
- theta: roll as Euler angle
- psi:   yaw as Euler angle

Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>
  José Roberto Colombo Junior <colombojrj[at]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

  Created on: 10/05/2015

"""

# Load the necessary libraries
import numpy as np
from nav import angle2dcm # angle2dcm forms the transformation from NED
                          # to body using the 321 Euler angle sequence

def magnetometer_yaw(pitch, roll, mag_read, input_units='rad'):
    """
    Derive yaw from body-measured magnetometer measurements.  A rotation
    sequence of 3-2-1 is assumed for the Euler angles.  The pitch and roll are
    necessary to level the body-axes magnetometer measurements before computing
    the associated yaw angle.
    
    Parameters
    ----------
    pitch : pitch angle, units of input_units.
    roll  : roll angle , units of input_units.
    mag_read: body-axes magnetometer measurements (Gauss)
    input_units: units for input angles {'rad', 'deg'}, optional.


    Returns
    -------
    yaw: magnetometer derived yaw angle (radians)
    
    Note
    ----
    TODO: Add tests for this method.
    """
    
    # Extract data from vector
    hx = mag_read[0]
    hy = mag_read[1]
    hz = mag_read[2]
    
    # Apply necessary unit transformations.
    if input_units == 'rad':
        pitch_rad, roll_rad = pitch, roll
    elif input_units == 'deg':
        pitch_rad, roll_rad = np.radians([pitch, roll])
        
    # Transform the body-axes magnetometer measurements to North-East plane
    # (I believe this is the wander-azimuth plane)
    
    # If input is an iterable (list/array), we need a loop, otherwise handle as 
    # a scalar.  We do a naiive check on only hx, and assume all other inputs
    # are the same type/length
    if np.iterable(hx):
        drl = len(hx)
        mag_wander = np.nan * np.zeros((drl, 3)) # declare storage for levelled magnetometer measurements
        for k, [p, r, hx_k, hy_k, hz_k] in enumerate(zip(pitch_rad, roll_rad, hx, hy, hz)):
            Rw2b = angle2dcm(0.0, p, r) # transform wander --> body
            Rb2w = np.array(Rw2b.T)
    
            mag_wander[k,:] = np.dot(Rb2w, [hx_k, hy_k, hz_k])   
            
        hx_level, hy_level = mag_wander[:,0], mag_wander[:,1]
    else:
        Rw2b = angle2dcm(0.0, pitch_rad, roll_rad) # transform wander --> body
        Rb2w = np.array(Rw2b.T)

        mag_wander = np.dot(Rb2w, [hx, hy, hz])
        
        hx_level, hy_level = mag_wander[0], mag_wander[1]
        
    
    # Compute yaw angle from the leveled magnetometer measurements
    yaw_mag = -np.arctan2(hy_level, hx_level) # (radians)
    
    return yaw_mag

def accelerometer_PQ(acc_read):
    """
    Assuming no accleration, one may derive pitch and roll (on the body frame) based on the measured
    gravitational accleratation vector.
    
    Parameters
    ----------
    acc_read: array with accelerometer measurements (ax, ay, az, in m/s^2)
    
    Returns
    -------
    P and Q (radians), which are the PITCH and ROLL on the body frame
    
    Note
	# TODO: Add tests for this method
    """
    
    # Extract data from accelerometer readings
    ax = acc_read[0]
    ay = acc_read[1]
    az = acc_read[2]
    
    # Convert acceleration measurements from m/s^2 to to g's
    g = -9.81 # m/s^2
    ax_g, ay_g, az_g = ax/g, ay/g, az/g
    
    # Both are in units of degrees
    Q = np.arctan2(ay_g, az_g)
    P = np.arctan2(-ax_g, np.sqrt(ay_g**2 + az_g**2))
    
    return accel_pitch, accel_roll

    
