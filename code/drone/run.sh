#!/bin/bash

# Change CPU governor to "performance"
cpufreq-set -g performance

# Execute the controller as a high priority proccess
nice -n -20 python main.py
