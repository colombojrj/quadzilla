#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to work with some accelerometers:
 -> BMP085, from  Bosch

It is assumed that the sensor is connected via I2C

Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>222
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 18/11/2015

"""

# Loading the necessary libraries
#import smbus
from numpy import array
from time import sleep

class BMP085:
  """
  Class: BMP085
  File: barometer.py
  Purpose: Python library to work with BMP085, from Bosch
  Created on: 24/10/2014
  Based from: https://github.com/emcconville/BMP085 
  """

  def __init__(self, bus, OPERATION_MODE="STANDARD", debug=False):
    """
    BMP085 constructor

    __init__(bus, OPERATION_MODE, RANGE, FILTER, debug=False)

    Parameters
    ----------
    bus : I2C bus object (where the sensor is connected to)
    
    OPERATION_MODE : string specifying the desired operation mode (default: "LOW_NOISE")
    Possible choices: LOW_NOISE and LOW_POWER

    RANGE : string specifying the range (default: "RANGE_2G")
    Possible choices: RANGE_1G, RANGE_1G5, RANGE_2G, RANGE_3G, RANGE_4G,
                      RANGE_8G and RANGE_16G

    FILTER : string specifying which filter configuration must be used (default: "LOW_300Hz")
    Possible choices: LOW_10Hz, LOW_20Hz, LOW_40Hz, LOW_75Hz, LOW_150Hz, 
                      LOW_300Hz, LOW_600Hz, LOW_1200Hz, HIGH_PASS_1Hz
    
    debug : Show debug information?
    Possible choices: True or False
  
    """

    # Keep bus information as internal variable
    self.bus = bus

    # Some useful BMP085 definitions
    self.CHIP = 0x77

    # Config mode definition
    self.CONFIG_MODE_CODE={"ULTRA_LOW_POWER"       : 0 ,
                           "STANDARD"              : 1 ,
                           "HIGH_RESOLUTION"       : 2 ,
                           "ULTRA_HIGH_RESOLUTION" : 3 }

    self.CONFIG_MODE_NAME={"ULTRA_LOW_POWER"       : "Ultra low power" ,
                           "STANDARD"              : "Standard",
                           "HIGH_RESOLUTION"       : "High resolution",
                           "ULTRA_HIGH_RESOLUTION" : "Ultra High resolution" }
    
    self.WAIT_TIME={"ULTRA_LOW_POWER"       : 4.5e-3 ,
                    "STANDARD"              : 7.5e-3 ,
                    "HIGH_RESOLUTION"       : 13.5e-3,
                    "ULTRA_HIGH_RESOLUTION" : 25.5e-3}

    # Make this variable part of the object
    self.OPERATION_MODE = OPERATION_MODE
    if debug:
      print "Selected operation mode:", self.CONFIG_MODE_NAME[self.OPERATION_MODE]

    # Load calibration coefficients
    self.cal = self.__read_calibration()
    
  def writeByte(self,address,value):
    self.bus.write_byte_data(self.CHIP,address,value)

  def readByte(self,address):
    byte = self.readUnsignedByte(address)
    if byte > 0x7f:
      byte = byte - 0x100
    return byte

  def readUnsignedByte(self,address):
    return self.bus.read_byte_data(self.CHIP,address)

  def readInt(self,address):
    msb = self.readByte(address)
    lsb = self.readUnsignedByte(address+1)
    return ( msb << 0x08 ) + lsb

  def readUnsignedInt(self,address):
    msb = self.readUnsignedByte(address)
    lsb = self.readUnsignedByte(address+1)
    return ( msb << 0x08 ) + lsb

  def __read_int16(self, MSB, LSB):
    return (MSB << 8) + LSB

  def __read_calibration(self):
    self.AC1 = self.readInt(0xAA)
    self.AC2 = self.readInt(0xAC)
    self.AC3 = self.readInt(0xAE)
    self.AC4 = self.readUnsignedInt(0xB0)
    self.AC5 = self.readUnsignedInt(0xB2)
    self.AC6 = self.readUnsignedInt(0xB4)
    self.B1  = self.readInt(0xB6)
    self.B2  = self.readInt(0xB8)
    self.MB  = self.readInt(0xBA)
    self.MC  = self.readInt(0xBC)
    self.MD  = self.readInt(0xBE)    

  def __calculate_temperature(self, temperature):
    x1      = (((temperature - self.AC6) * self.AC5) >> 0x0F)
    x2      = ((self.MC << 0x0B)/(x1 + self.MD))
    self.B5 = x1 + x2
    return ((self.B5+8)>>4)/10.0

  def __correct_pressure(self,raw_pressure,temperature,debug=False):
    if debug:
      print 'Temperature:', temperature

    self.B6 = self.B5 -4000
    x1 = (self.B2 * (self.B6 * self.B6) >> 12) >> 11
    x2 = (self.AC2 * self.B6) >> 11
    x3 = x1 + x2
    self.B3 = (((self.AC1 * 4 + x3) << self.CONFIG_MODE_CODE[self.OPERATION_MODE]) + 2) / 0x04
    
    x1 = (self.AC3 * self.B6) >> 0x0D
    x2 = (self.B1 * ((self.B6 * self.B6) >> 0x0C)) >> 0x10
    x3 = ((x1 + x2) + 2) >> 0x02
    self.B4 = (self.AC4 * (x3 + 0x8000)) >> 0x0F
    self.B7 = ((raw_pressure - self.B3) * (0xC350 >> self.CONFIG_MODE_CODE[self.OPERATION_MODE]))
    if self.B7 < 0x80000000 :
      p = (self.B7 * 0x02) / self.B4
    else :
      p = (self.B7 / self.B4) * 0x02
    x1 = (p >> 0x08) * (p >> 0x08)
    x1 = (x1 * 0x0BDE) >> 0x10
    x2 = (-0x1CBD * p) >> 0x10
    p  = p + (( x1 + x2 + 0x0ECF ) >> 0x04 )
    return p/100.

  def __calculate_altitude(self, pressure):
    p0 = 1013.25
    return 44330.*(1-(pressure/p0)**(1/5.255))

  def read(self, return_raw_data=False, debug=False):
    """
    bar = read (return_raw_data=False, debug=False)
    
    It read the altitude (meters)

    Parameters
    ----------
    return_raw_data : return raw data or acceleration data? (default: False)
    debug : debug flag (default: False)

    Returns
    -------
    bar : altitude (meters)

    """

    # Read uncompensated temperature from sensor
    self.writeByte(0xF4,0x2E)
    sleep(0.005) # Wait for it
    raw_temperature = self.readUnsignedInt(0xF6)
    temperature = self.__calculate_temperature(raw_temperature)

    # Request pressure
    self.writeByte(0xF4,0x34 + (self.CONFIG_MODE_CODE[self.OPERATION_MODE]<<0x06))
    sleep(self.WAIT_TIME[self.OPERATION_MODE])
    msb = self.readUnsignedByte(0xF6)
    lsb  = self.readUnsignedByte(0xF7)
    xlsb = self.readUnsignedByte(0xF8)
    msb <<= 16
    lsb <<= 8
    raw = (msb +lsb +xlsb)>>(0x08 -self.CONFIG_MODE_CODE[self.OPERATION_MODE])

    if debug:
      print raw

    if return_raw_data:
      return raw
    else:
      pressure = self.__correct_pressure(raw, temperature)
      altitude = self.__calculate_altitude(pressure)
      data = [temperature, pressure, altitude]
      return data

def main():
  import smbus
  bus = smbus.SMBus(1)
  barometer = BMP085(bus, OPERATION_MODE="ULTRA_HIGH_RESOLUTION", debug = True)
  data = barometer.read()
  print 'Temperature:', data[0], "oC"
  print 'Pressure:   ', data[1], "hPa"
  print 'Altitude:   ', data[2], "meters (absolute measurement)"
  bus.close()
  return 0

if __name__ == '__main__':
	main()

