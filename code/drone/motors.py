#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to run the four brushless motors.


It is assumed that the motors are controlled via PWM

Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>
  José Roberto Colombo Junior <colombojrj[at]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 03/03/2015

"""

# Loads the necessary libraries
import Adafruit_BBIO.PWM as PWM
import time
import numpy as np

#
# How to install the Adafruit_BBIO library
# https://learn.adafruit.com/setting-up-io-python-library-on-beaglebone-black/installation-on-ubuntu
#

# Pins that can operate as PWM generator
# "P8_13", EHRPWM2B, ehrpwm.2:1
# "P8_19", EHRPWM2A, ehrpwm.2:0
# "P9_14", EHRPWM1A, ehrpwm.1:0
# "P9_16", EHRPWM1B, ehrpwm.1:1
# "P9_31", SPI1_SCLK ehrpwm.0:0
# "P9_29", SPI1_D0   ehrpwm.0:1
# "P9_42", GPIO0_7   ecap.0
# "P9_28", SPI1_CS0  ecap.2


class ESC:
  """
  File: motors.py
  Purpose: Python library to work with the ESCs speed controllers. Supports 
		only ESCs with the Simonk firmware.
  Created on: 03/03/2015
  """

  def __init__(self, FREQUENCY=50, motors=[],ka=[],offset=[],debug=False):
    """
    ESC constructor
	
    __init__(self, FREQUENCY="50", motors=["P9_42", "P8_13", "P9_14", "P9_28"]):

    Parameters
    ----------
    FREQUENCY: integer specifying the frequency of the PWM (default: 50 Hz)
    motors: string specifying the pinout of the PWM output for the motor "m1" (default: "P9_42" for beaglebone white)
    
    """
    
    # Set debug flag
    self.debug = debug
    
    # Set pwm_min and pwm_max (default of Simonk firmware)
    self.pulse_min = 1060.0 # us
    self.pulse_max = 1860.0 # us
    #self.pulse_min = 0.
    #self.pulse_max = 2000.

    # Make input arguments to be part of class
    self.FREQUENCY = FREQUENCY
    if motors == []:
      self.motors = {}
      self.motors[0] = "P9_42"
      self.motors[1] = "P8_13"
      self.motors[2] = "P9_14"
      self.motors[3] = "P9_28"
    else:
      self.motors = motors
    
    # Motors parameters
    if ka == []:
      self.ka = [0.9121, 0.96, 0.9294, 0.9655]
    else:
      self.ka = ka
    if offset == []:
      self.offset = [965.0458, 954.9895, 958.9435, 952.8626]
    else:
      self.offset = offset
    
    # Number of motors
    self.n_motors = len(self.motors)
    
    # Some useful constants
    self.duty2length = (1e6/self.FREQUENCY)/100. # duty to length (microseconds)
    self.length2duty = 1./self.duty2length
  
    # Time sleep between the change of duty cycle during the threshold
    delay = 0.1 # seconds

    # Starting the communication with the ESCs
    for k in xrange(0, self.n_motors):
      PWM.start(self.motors[k], 0, self.FREQUENCY)

    # Set pulse length of 1000us (MultiWii)
    self.write_length([1000]*self.n_motors)


  def w2duty(self, w):
    """
    w2duty(w)
    
    Function to convert angular velocity w (rad/s) to PWM duty cycle
    
    Parameters:
    w: [w1, w2, w3, w4] (in rad/s)
    
    Returns:
    duty: [duty1, duty2, duty3, duty4] (in %)
    """
    
    # For each motor
    duty = [(self.ka[i]*w[i]+self.offset[i])*self.length2duty for i in xrange(0, self.n_motors)]

    if self.debug:
      print 'Duty cycle before saturation:', duty
      
    return duty

  def write_w(self, w = [0, 0, 0, 0]):
    """
    write_w(self, w = [0, 0, 0, 0])
    
    This function receives the w (rad/s) value and automatically converts it
    to the proper duty cycle of the motors.

    Parameters
    ----------
    w: array with the four values of w (rad/s) for each motor 
       (default: [0,0,0,0])

    """
    
    if self.debug:
      print 'nMotors:', self.n_motors
    duty_array = self.w2duty(w)
    self.write_duty(duty_array)
#    for k in xrange(0, self.n_motors):
#      PWM.set_duty_cycle(self.motors[k], duty_array[k])

  def write_duty(self, duty = [0, 0, 0, 0]):
    """ 
    write_pwm(self, duty = [0, 0, 0, 0])

    Function that writes the PWM directly to the motors.
  
    Parameters
    ----------
    pwm: array with tha values of duty cycle of PWM
    
    """

    # The PWM duty must remain inside the range programmed in Simonk ESC
    for i in xrange(0, self.n_motors):
      if duty[i]*self.duty2length > self.pulse_max:
        duty[i] = self.pulse_max*self.length2duty
      if duty[i]*self.duty2length < self.pulse_min:
        duty[i] = self.pulse_min*self.length2duty
      PWM.set_duty_cycle(self.motors[i], duty[i])
    
  def write_length(self, lengths = [0, 0, 0, 0]):
    """
    Write_length(self, lengths = [0, 0, 0, 0])
	
    Function to apply the values of pulse lenths to the motors
	
    Parameters
    ----------
    length: array with tha values of length of pulses
    """	
    
    duty_cycles = [item*self.length2duty for item in lengths]
    self.write_duty(duty_cycles)
    
  def stop_all(self):
    """
    Function to stop the all motors immediately

    """
    
    for k in xrange(0, self.n_motors):
      PWM.set_duty_cycle(self.motors[k], self.pulse_min*self.length2duty)
    PWM.cleanup()

  def stop(self, motors):
    """
    Function to stop a specifc motor/motors immediately
    
    Parameters
    ----------
    motors: array with strings (Example: motors = [1, 2])
    
    """
    
    for k in xrange(0,len(motors)):
        PWM.stop(self.motors[k])
        PWM.set_duty_cycle(self.motors[k], self.pulse_min*self.length2duty)

def main():
  from motors import ESC
  import time
  m = ESC(FREQUENCY = 50.)
  
  m.write_w([150]*4)  

  time.sleep(3)
  m.write_length([1000]*4)

  return 0

if __name__ == '__main__':
  main()

