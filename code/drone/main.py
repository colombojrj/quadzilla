# -*- coding: utf-8 -*-
"""
Main file to run the controller and state estimator

Authors:
    Arthur Gagg <luizarthur.gagg[AT]gmail.com>
    José Roberto Colombo Junior <colombojrj[AT]gmail.com>
    Pedro Augusto Queiroz de Assis <pedroaugusto.feis[AT]gmail.com>
"""

# Load libraries
from model import *
from turnon import *
from util import *
from state_observer import *
import controller
import numpy as np
import smbus, time, motors, serial
import accelerometer, barometer, compass, gyroscope
from scipy.io import savemat
from communication import decode

# Start serial communications
ser = serial.Serial('/dev/ttyO5', 115200, timeout=0.01)
ser.open()

# defining the bus
bus = smbus.SMBus(1)

# initalizing the sensors class
mag = compass.HCM5883(bus, offset_x = 241.1497, offset_y= 161.2128)
gyro = gyroscope.ITG_3200(bus)
barom = barometer.BMP085(bus)
acc = accelerometer.BMA180(bus, RANGE ='RANGE_2G', FILTER = "LOW_150Hz")

# initalizing the motors class
motors = motors.ESC(FREQUENCY = 400)

# Load quadcopter parameters
parameters = LoadVars()
Ts = 10e-3              # sampling time

# Initialize the complementary filter
a_pitch = 0.99
a_roll  = 0.99
a_yaw   = 0.5
g = 10.05
cFilter = ComplementaryFilter(Ts, a_pitch, a_roll, a_yaw, g)

# Turnin on the quad
gyro_bias, pitch_bias, roll_bias, yaw_bias = turnon(bus, parameters, kend=50)
#pitch_bias = 0.
#roll_bias  = 0.
#yaw_bias = compass.yaw(mag.read()) # Read yaw bias

# Configure the simulation
tend = 90.
kend = int(tend/Ts)

# Log and auxiliary variables
nx = 6
nu = 4
ny = 6
u_log = np.zeros((nu, kend))
u_pitch = np.zeros((1, kend))
u_roll = np.zeros((1, kend))
u_yaw = np.zeros((1, kend))
u = np.matrix(np.zeros((nu,1)))

# Log variables of the complementary filter
pitchf = np.zeros((kend,))
rollf = np.zeros((kend,))
yawf = np.zeros((kend,))

total_time = np.zeros((1, kend))   # Time spent during the control loop

acc_log = np.zeros((3,kend))       # Accelerometer raw readings
gyro_log = np.zeros((3,kend))      # Gyroscope raw readings
compass_raw = np.zeros((3,kend))   # Compass raw readings
pitch_read = np.zeros((1,kend))    # Raw pitch readings
roll_read  = np.zeros((1,kend))    # Raw roll readings
pitch_log = np.zeros((1,kend))     # Computed pitch readings
roll_log = np.zeros((1,kend))      # Computed roll readings
yaw_log = np.zeros((1,kend))       # Computed yaw readings

pitch_ref_log = np.zeros((1,kend)) # Reference of pitch
roll_ref_log = np.zeros((1,kend))  # Reference of roll
yaw_ref_log = np.zeros((1,kend))   # Reference of roll
dpitch_ref_log = np.zeros((1,kend))# Reference of pitch velocity
droll_ref_log = np.zeros((1,kend)) # Reference of roll velocity
dyaw_ref_log = np.zeros((1,kend))  # Reference of roll velocity

pitch_err = np.zeros((1,kend))  # error of pitch
roll_err = np.zeros((1,kend))   # error of roll
yaw_err = np.zeros((1,kend))    # error of yaw
dpitch_err = np.zeros((1,kend)) # error of pitch velocity
droll_err = np.zeros((1,kend))  # error of roll velocity
dyaw_err = np.zeros((1,kend))   # error of yaw velocity

pitch_bias_kalman = np.zeros((1,kend))  # error of pitch
roll_bias_kalman = np.zeros((1,kend))  # error of pitch

w0_log = np.zeros((1,kend))
xh_log = np.zeros((nx,kend))       # Estimated state readings

# PID constants
# Attitude control
Kp_pitch = 0.8
Kp_roll  = 0.8
Kp_yaw   = 1.1
Ki_pitch = 0.03
Ki_roll  = 0.03
Ki_yaw   = 0.
Kd_pitch = 0.
Kd_roll  = 0.
Kd_yaw   = 0.

# Rate control
Kp_dpitch = 50.
Kp_droll  = 50.
Kp_dyaw   = 80.
Ki_dpitch = 1.
Ki_droll  = 1.
Ki_dyaw   = 0.
Kd_dpitch = 3.
Kd_droll  = 3.
Kd_dyaw   = 3.

# Maximum error
emax = 45. * np.pi/180.

# Conversion from raw communication to useful data
c_pitch = (np.pi/180)*45./127.
c_roll = (np.pi/180)*45./127.
c_yaw = (np.pi/180)*45./127.
c_w0 = 700./127.
DATA_LEN = 7

# Create the six PID objects
w_cut = 10 /(1/Ts/2) # cut frequency of low pass filter
PID_pitch = controller.PID(Kp_pitch, Ki_pitch, Kd_pitch, Ts, wn=w_cut)
PID_roll = controller.PID(Kp_roll, Ki_roll, Kd_roll, Ts, wn=w_cut)
PID_yaw = controller.PID(Kp_yaw, Ki_yaw, Kd_yaw, Ts, wn=w_cut)
PID_dpitch = controller.PID(Kp_dpitch, Ki_dpitch, Kd_dpitch, Ts, wn=w_cut)
PID_droll = controller.PID(Kp_droll, Ki_droll, Kd_droll, Ts, wn=w_cut)
PID_dyaw = controller.PID(Kp_dyaw, Ki_dyaw, Kd_dyaw, Ts, wn=w_cut)

w0 = 200

# References
pitch_ref = 0.
roll_ref  = 0.
yaw_ref = 0.
dpitch_ref = 0.
droll_ref  = 0.
dyaw_ref = 0.

#testing the different approach to estimatate euler angles
Ad = np.matrix([[1., 0., -Ts, 0],
                [0., 1., 0., -Ts],
                [0., 0., 1., 0.],
                [0., 0., 0., 1.]])

Bd = np.matrix([[Ts, 0.],
               [0., Ts],
               [0., 0.],
               [0., 0.]])
C = np.matrix([[1., 0., 0., 0.],
               [0., 1., 0., 0.]])
Pw = np.matrix(np.diag([2.,2.,0.01,0.01]))
Pv = np.matrix(np.diag([80,80]))
xh_kalman = np.matrix([[0.],
               [0.],
               [0.],
               [0.]])

L = Kalman_design(Ad, C, Pw, Pv, kend = 500)

# Run the simulation
try:
  wait = 0.1
  # Warming up the motors
  print "Warming up the motors!"
  motors.write_w([w0*0.5, w0*0.5 , w0*0.5 , w0*0.5])
  time.sleep(wait)
  motors.write_w([w0*0.67, w0*0.67 , w0*0.67 , w0*0.67])
  time.sleep(wait)
  motors.write_w([w0*0.84, w0*0.84 , w0*0.84 , w0*0.84])
  time.sleep(wait)
  motors.write_w([w0, w0, w0, w0])
  time.sleep(wait)

  # Print a message informing that the control loop is now active
  print "I started the experiment..."

  for k in xrange(0, kend):
    # Start counting time
    t_start = time.time()

    # Read sensors
    gyro_m = gyro.read() -gyro_bias
    acc_readings = acc.read()
    acc_log[:, k] = acc_readings.squeeze()
    pitch, roll =  accelerometer.pitchroll(acc_readings, parameters['g'])
    pitch_read[0, k] = pitch -pitch_bias # Store the raw pitch readings
    roll_read[0, k] = roll   -roll_bias  # Store the raw roll readings
    gyro_log[:, k] = gyro_m.squeeze()
    mag_m = mag.read()
    yaw = compass.yaw(mag_m) -yaw_bias
    yaw_log[0, k] = yaw
    compass_raw[:, k] = mag_m.squeeze()

    # Use complementary filter to improve pitch, roll and yaw angles    
    pitchf[k], rollf[k], yawf[k] = cFilter.UpdateFilter(pitch, roll, yaw, gyro_m)
    
    # Apply the Kalman filter to the raw data
    #y = np.matrix([[gyro_m[1]],[gyro_m[0]],[-gyro_m[2]],[roll],[pitch],[yaw]])
    y = np.matrix([[gyro_m[1]],[gyro_m[0]],[-gyro_m[2]],[rollf[k]],[pitchf[k]],[yawf[k]]])
    #xnew = Ad*xh
    #xh= xnew +Bd*u*0 +L*(y -xnew)
    #xh_log[:,k] = xh.squeeze()
    u_kalman = np.matrix([[gyro_m[0]],[gyro_m[1]]])
    y_kalman = np.matrix([[pitch_read[0, k]],[roll_read[0, k]]])
    xh_kalman = estimate_corrform(Ad, Bd, C, L, xh_kalman, u_kalman, y_kalman)
    pitch_bias_kalman[:,k] = xh_kalman[2]
    roll_bias_kalman[:,k] = xh_kalman[3]
    xh = np.matrix([[gyro_m[1]-xh_kalman[2]],
                    [gyro_m[0]-xh_kalman[3]],
                    [-gyro_m[2]],
                    [xh_kalman[1]],
                    [xh_kalman[0]],
                    [yawf[k]]])
    
    xh_log[:,k] = xh.squeeze()
    
    ## Updating control signals
    # Attitude control update
#    dpitch_ref_log[0,k] = PID_pitch.calculate_control(xh_log[4,k], pitch_ref)
#    droll_ref_log[0,k] = PID_roll.calculate_control(xh_log[3,k], roll_ref)
#    dyaw_ref_log[0,k] = PID_yaw.calculate_control(xh_log[5,k], yaw_ref)
#    
#    P, Q, R = AngleRateEF2AngleRateBF(xh_log[4,k],         # pitch
#                                      xh_log[3,k],         # roll
#                                      xh_log[5,k],         # yaw
#                                      dpitch_ref_log[0,k], 
#                                      droll_ref_log[0,k], 
#                                      dyaw_ref_log[0,k])

    # Rate control update
    u_pitch[0,k] = PID_dpitch.calculate_control(xh_log[1,k], pitch_ref)
    u_roll[0,k] = PID_droll.calculate_control(xh_log[0,k], roll_ref)
    u_yaw[0,k] = PID_dyaw.calculate_control(xh_log[2,k], yaw_ref) 
    #u_pitch[0,k] = PID_dpitch.calculate_control(xh_log[1,k], Q)
    #u_roll[0,k] = PID_droll.calculate_control(xh_log[0,k], P)
    #u_yaw[0,k] = PID_dyaw.calculate_control(xh_log[2,k], R)
    
    # Create the control vector
    u = np.matrix([[ u_pitch[0, k] +u_yaw[0, k]],
                   [ u_roll[0, k]  -u_yaw[0, k]],
                   [-u_pitch[0, k] +u_yaw[0, k]],
                   [-u_roll[0, k]  -u_yaw[0, k]]])
    u_log[:,k] = u.squeeze()
    
    # Apply the control to the motors
    motors.write_w(np.array(u).squeeze()+np.array([w0, w0, w0, w0]))

    # Read serial data
    inData = ser.inWaiting()
    if inData == DATA_LEN:
      serial_data = ser.read(DATA_LEN)
      refs = decode(serial_data, DATA_LEN)
      if refs['error'] == 0:
        pitch_ref = refs['pitch']*c_pitch
        roll_ref = refs['roll']*c_roll
        yaw_ref = refs['yaw']*c_yaw
        w0 = refs['throttle']*c_w0
        if refs['abort'] == 1:
          raise NameError('Abort command!')
    elif inData > DATA_LEN:
      ser.flushInput()

    # Update the reference vector log
    pitch_ref_log[0, k] = pitch_ref
    roll_ref_log[0, k] = roll_ref
    yaw_ref_log[0, k] = yaw_ref
    w0_log[0, k] = w0

    # Wait for next sampling period
    elapsed_time = time.time() -t_start
    total_time[0, k] = elapsed_time
    time_to_wait = Ts -elapsed_time
    if time_to_wait > 0:
      time.sleep(time_to_wait)

  # Display a final messsage
  print "The experiment is done. I hope you are happy with the results :-)"

except (KeyboardInterrupt, NameError):
  motors.write_w([0] * 4)
  print "Experiment aborted!!! I hope you are fine :O"

# Stop motors and close serial and I2C bus
motors.write_w([0]*4)
bus.close()
ser.close()

filename = time.ctime()
filename = filename[4:].replace(' ', '_') + '.mat'
savemat(filename,{'acc':acc_log, 'gyro':gyro_log, 'compass_raw':compass_raw, 'w0':w0_log,
                  'u':u_log, 'time':total_time,
                  'pitch':pitch_read, 'roll':roll_read,
                  'dpitch_ref':dpitch_ref_log, 'droll_ref':droll_ref_log,'dyaw_ref':dyaw_ref_log,
                  'pitch_ref': pitch_ref_log, 'roll_ref': roll_ref_log, 'yaw_ref': yaw_ref_log,
                  'u_pitch':u_pitch, 'u_roll':u_roll, 'u_yaw':u_yaw,
                  'xh':xh_log, 'yaw':yaw_log,
                  'pitchf':pitchf, 'rollf':rollf, 'yawf':yawf, 'roll_bias_kalman':roll_bias_kalman, 'pitch_bias_kalman':pitch_bias_kalman})
