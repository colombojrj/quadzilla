#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to initialize the stuff of the quad. By stuff we mean: sensors,
motors, state observer. Furthermore, we do this to remove the bias of each sensor...


Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>
  José Roberto Colombo Junior <colombojrj[at]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

inputs
kend: number of samples to determine the bias of each sensor. The deafult is 500

Created on: 18/01/2016

"""
# Loads the libraries of the sensors
import accelerometer, barometer, compass, gyroscope
import numpy as np

def turnon(bus, p, kend=500):

  mag = compass.HCM5883(bus)
  gyro = gyroscope.ITG_3200(bus)
  #barom = barometer.BMP085(bus)
  acc = accelerometer.BMA180(bus)

  # scopes
  pitch = np.zeros((1,kend))
  roll  = np.zeros((1,kend))
  gyro_m = np.zeros((3,kend))
  #baro_m = np.zeros((kend))
  mag_yaw_m = np.zeros((kend))

#ok now I ill pick up some measurements and define the bias of each sensor
  for k in xrange(0, kend):
    pitch[0,k], roll[0,k] = accelerometer.pitchroll(acc.read(), p['g'])
    gyro_m[:,k] = gyro.read()
    #baro_m[k] = barom.read()[2]
    mag_yaw_m[k] = compass.yaw(mag.read())

  gyro_bias = np.array([np.mean(gyro_m[0,:]), np.mean(gyro_m[1,:]), \
  np.mean(gyro_m[2,:])])
  
  pitch_bias = np.mean(pitch)
  roll_bias  = np.mean(roll)

  mag_yaw_bias = np.mean(mag_yaw_m)
  #baro_bias = np.mean(baro_m)
  
  #return gyro_bias, pitch_bias, roll_bias, baro_bias, mag_yaw_bias
  return gyro_bias, pitch_bias, roll_bias, mag_yaw_bias
  
  
def main():
  import smbus
  bus = smbus.SMBus(1)
  p = {}
  p['g'] = 10.05
  print turnon(bus, p, 100)
  
  return 0

if __name__ == '__main__':
        main()  
  
  
