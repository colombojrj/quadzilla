#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: José Roberto Colombo Junior
# Date:   24/04/2015
# Where:  Instituto Tecnológico de Aeronáutica

# Necessary to make this library compatible with Python 2/3
from __future__ import print_function, division

# Import some libraries 
import numpy as np
from scipy.linalg import expm, solve_discrete_are, norm
from scipy.misc import factorial

def dlqr(A, B, Q, R):
    """
    Solve the discrete time LQR controller.
         
    x[k+1] = A x[k] + B u[k]
     
    J = sum (x[k].T*Q*x[k] + u[k].T*R*u[k])
    """
 
    # First, solve the Ricatti equation
    P = solve_discrete_are(A, B, Q, R)

    # Compute the LQR gain
    if type(A) == type(np.array([1])):
      K = np.linalg.inv(R + np.dot(B.T,P.dot(B))).dot(B.T.dot(P).dot(A))
    else:
      P = np.matrix(P)
      K = np.linalg.inv(R + B.T*P*B) * B.T*P*A
     
    return K, P

# Is the number even?
def isEven(number):
  return not number % 2

# Discretization method
def c2dm(A, B, Ts, debug=False):
    """
    Inputs:
    - matrix A, stored as dictionary
    - matrix B, stored as dictionary
    - Sampling time Ts
    
    Returns:
    - matrix Ad
    - matrix Bd
    """
    
    # Number of vertices
    nv = len(A)
    
    # Number of state variables and control channels
    nx = np.shape(A[0])[0]
    nu = np.shape(B[0])[1]

    # Stop criterion    
    criterion = 1e-6
    
    Ad = {}; Bd = {}
    for i in range(0, nv):
      if np.linalg.det(A[i]) != 0:
        Ad[i] = expm(A[i]*Ts)
        Bd[i] = np.linalg.inv(A[i])*(Ad[i]-np.eye(nx))*B[i]
      else:
        S_old = np.zeros((nx,nx))
        S = np.zeros((nx,nx))
        delta = 1 # Initial error
        n = 0     # Number of iterations until convergence
        Atemp = np.eye(nx)
        while delta > criterion:
          S = S_old + Atemp*(Ts**(n+1))/factorial(n+1)          
          n = n +1
          delta = norm(abs(S) -abs(S_old))
          # Housekeeping
          S_old = S
          Atemp = Atemp * A[i]
          if debug:
            print('iteration number:', n)
        Ad[i] = np.eye(nx)+A[i]*S
        Bd[i] = S*B[i]
    return Ad, Bd

def AngleRateEF2AngleRateBF(pitch, roll, yaw, dpitch, droll, dyaw):
  '''
  This function converts earth frame angle rate to body frame angle rate
  '''
  cosp = np.cos(pitch)
  cosr = np.cos(roll)
  sinr = np.sin(roll)
  P = droll -np.sin(pitch)*dyaw
  Q = cosr*dpitch +sinr*cosp*dyaw
  R = -sinr*dpitch +cosr*cosp*dyaw
  
  return P, Q, R

def test_c2dm():
  A = {0 : np.matrix([[1,2],[3,4]]), 1 : np.matrix([[1,2],[3,6]])}
  B = {0 : np.matrix([[5],[6]]), 1 : np.matrix([[5],[6]])}
  Ts= 1
  Ad, Bd = c2dm(A,B,Ts,debug=False)
  print ('A:', Ad[1], '\nB:', Bd[1])

# Complementary filter class
class ComplementaryFilter():
  """
  Class: ComplementaryFilter
  File: util.py
  Purpose: Python library to implement a complementary filter
  Created on: 10/07/2016
  """
  
  def __init__(self, dt, a_pitch, a_roll, a_yaw, g = 9.81):
    self.dt      = dt
    self.a_pitch = a_pitch
    self.a_roll  = a_roll
    self.a_yaw   = a_yaw
    self.pitch   = 0.
    self.roll    = 0.
    self.yaw     = 0.
    self.g       = g
    
  def UpdateFilter(self, pitch, roll, yaw, gyro_readings):
    """
    Function to update the filter output
    """
    
    # Equation to be implemented:
    # angle = a*(angle + gyro*dt) + (1-a)*accelerometer_angle
    self.pitch = self.a_pitch*(self.pitch + gyro_readings[0]*self.dt) +(1-self.a_pitch)*pitch
    self.roll = self.a_roll*(self.roll + gyro_readings[1]*self.dt) +(1-self.a_roll)*roll
    self.yaw = self.a_yaw*(self.yaw -gyro_readings[2]*self.dt) +(1-self.a_yaw)*yaw
    
    return self.pitch, self.roll, self.yaw


if __name__ == '__main__':
  test_c2dm()
