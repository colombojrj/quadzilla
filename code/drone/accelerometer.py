#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python library to work with some accelerometers:
 -> BMA180, from  Bosch
 -> ADXL345, from Analog Devices

It is assumed that the sensor is connected via I2C

Authors: 
  Arthur Gagg <luizarthur.gagg[at]gmail.com>
  José Roberto Colombo Junior <colombojrj[AT]gmail.com>
  Pedro Augusto Queiroz de Assis <pedroaugusto.feis[at]gmail.com>

Created on: 24/10/2014

"""

# Loading the necessary libraries
from numpy import array, arctan2, sqrt
import time
import numpy as np

# TODO
# Add orientation variables, i.e., x, y and z
def pitchroll(readings, g=9.81):
    """
    pitch, roll = pitchroll (readings, g=9.81)
    
    Returns a tuple with pitch and roll (pitch, roll).
    
    Inputs
    ------
    readings = accelerometer readings (read function)
    g = gravity acceleration (a real number, default is 9.81)
    
    Returns
    -------
    pitch (in radians)
    roll (in radians)
  
    """
        
    roll = arctan2(-readings[0], sqrt(readings[1]**2 + readings[2]**2))
    pitch = arctan2(readings[1], sqrt(readings[0]**2 + readings[2]**2))
    
    return pitch, roll

class BMA180:
  """
  Class: BMA180
  File: accelerometer.py
  Purpose: Python library to work with BMA180, from Bosch
  Created on: 24/10/2014
  """

  def __init__(self, bus, OPERATION_MODE="LOW_NOISE", RANGE="RANGE_2G", FILTER="LOW_300Hz", debug=False):
    """
    BMA180 constructor

    __init__(bus, OPERATION_MODE, RANGE, FILTER, debug=False)

    Parameters
    ----------
    bus : I2C bus object (where the sensor is connected to)
    
    OPERATION_MODE : string specifying the desired operation mode (default: "LOW_NOISE")
    Possible choices: LOW_NOISE and LOW_POWER

    RANGE : string specifying the range (default: "RANGE_2G")
    Possible choices: RANGE_1G, RANGE_1G5, RANGE_2G, RANGE_3G, RANGE_4G,
                      RANGE_8G and RANGE_16G

    FILTER : string specifying which filter configuration must be used (default: "LOW_300Hz")
    Possible choices: LOW_10Hz, LOW_20Hz, LOW_40Hz, LOW_75Hz, LOW_150Hz, 
                      LOW_300Hz, LOW_600Hz, LOW_1200Hz, HIGH_PASS_1Hz
    
    debug : Show debug information?
    Possible choices: True or False
  
    """

    # Keep bus information as internal variable
    self.bus = bus

    # Some useful BMA180 definitions
    self.ADDRESS = 0x40
    self.IDENTITY = 0x03

    # Sensiblity
    SENSIBILITY = {"RANGE_1G"  : 8192. ,
                   "RANGE_1G5" : 5460. ,
                   "RANGE_2G"  : 4096. ,
                   "RANGE_3G"  : 2730. ,
                   "RANGE_4G"  : 2048. ,
                   "RANGE_8G"  : 1024. ,
                   "RANGE_16G" : 512.  }

    # Possible configuration to range
    RANGE_CODE = {"RANGE_1G"  : 0b000 ,
                  "RANGE_1G5" : 0b001 ,
                  "RANGE_2G"  : 0b010 ,
                  "RANGE_3G"  : 0b011 ,
                  "RANGE_4G"  : 0b100 ,
                  "RANGE_8G"  : 0b101 ,
                  "RANGE_16G" : 0b110 }
    
    RANGE_NAME = {"0" : "1G"  ,
                  "1" : "1.5G",
                  "2" : "2G"  ,
                  "3" : "3G"  ,
                  "4" : "4G"  ,
                  "5" : "8G"  ,
                  "6" : "16G" }

    # Possible configuration to internal filter
    FILTER_CODE = {"LOW_10Hz"      : 0b0000 ,
                   "LOW_20Hz"      : 0b0001 ,
                   "LOW_40Hz"      : 0b0010 ,
                   "LOW_75Hz"      : 0b0011 ,
                   "LOW_150Hz"     : 0b0100 ,
                   "LOW_300Hz"     : 0b0101 ,
                   "LOW_600Hz"     : 0b0110 ,
                   "LOW_1200Hz"    : 0b0111 ,
                   "HIGH_PASS_1Hz" : 0b1000 }

    FILTER_NAME = {"0" : "Low-pass, f_cut=10Hz"  ,
                   "1" : "Low-pass, f_cut=20Hz"  ,
        		    "2" : "Low-pass, f_cut=40Hz"  ,
        		    "3" : "Low-pass, f_cut=75Hz"  ,
        		    "4" : "Low-pass, f_cut=150Hz" ,
        		    "5" : "Low-pass, f_cut=300Hz" ,
        		    "6" : "Low-pass, f_cut=600Hz" ,
        		    "7" : "Low-pass, f_cut=1200Hz",
        		    "8" : "High-pass, f_cut=1Hz"  }

    # Config mode definition
    CONFIG_MODE_CODE={"LOW_NOISE" : 0b00 ,
                      "LOW_POWER" : 0b11 }

    CONFIG_MODE_NAME={"0" : "Low-noise mode" ,
                      "3" : "Low-power mode" }

    # Translate the selected configuration
    self.OPERATION_MODE  = CONFIG_MODE_CODE[OPERATION_MODE]
    self.RANGE_SELECTED  = RANGE_CODE[RANGE]
    self.SENSIBILITY     = SENSIBILITY[RANGE]
    self.FILTER_SELECTED = FILTER_CODE[FILTER]

    # Check if BMA180 is properly working
    self.CHIP_ID = bus.read_word_data(self.ADDRESS, 0x00) & 0b111
    if (self.CHIP_ID != self.IDENTITY):
      print "**************************************************************"
      print "** Problem with accelerometer! Please, abort the operation! **"
      print "**************************************************************"
    else: # If it's working fine, then start setting the BMA180
      if debug:
        print "(OK) I found the accelerometer BMA180"

      ##################################
      # Restart the BMA180
      # From restart command the cpu should not use I2C the next 10us
      #bus.write_byte_data(self.ADDRESS, 0x10, 0xB6)
      #bus.write_byte(self.ADDRESS, 0x20)
      #time.sleep(1e-4) # wait a little more... will hurt no one 

      ######################################
      # EEPROM (and IMAGE) must allow write operations
      EE_W = (bus.read_word_data(self.ADDRESS, 0x0D) & 0b00010000) >> 4
      if (EE_W==0):
        bus.write_byte_data(self.ADDRESS, 0x0D, EE_W | 0b00010000) # allows wrinting
      if debug:
        EE_W = (bus.read_word_data(self.ADDRESS, 0x0D) & 0b00010000) >> 4
        if ((bus.read_word_data(self.ADDRESS, 0x0D) & 0b00010000) >> 4):
          print "(OK) BMA180 with unblock EEPROM"
        else:
          print "(EE) BMA180 with block EEPROM"
      
      ##################################
      # Operation mode configuration
      #
      # * TODO
      # * Implementar possibilidade de alterar o modo de operação do sensor. Datasheet, página 29
      MODE_DATA = bus.read_word_data(self.ADDRESS, 0x30) & 0b0011
      if debug:
        print "(**) Operation mode is set to:", CONFIG_MODE_NAME[str(MODE_DATA)]

      ########################################################
      # Set accelerometer to work with informed range
      RANGE_DATA = bus.read_word_data(self.ADDRESS, 0x35)
      if (((RANGE_DATA & 0b00001110) >> 1) != self.RANGE_SELECTED):
        RANGE_DATA = RANGE_DATA & 0xF1
        bus.write_byte_data(self.ADDRESS, 0x35, RANGE_DATA | (self.RANGE_SELECTED << 1))

      if debug:
        # Checks the range set
        RANGE_DATA = (bus.read_word_data(self.ADDRESS, 0x35) & 0b00001110)
        print "(**) Range is set to:", RANGE_NAME[str(RANGE_DATA >> 1)]

      #############################
      # Set accelerometer to work with informed filter
      FILTER_DATA = bus.read_word_data(self.ADDRESS, 0x20)
      if (((FILTER_DATA & 0b11110000) >> 4) != self.FILTER_SELECTED):
        FILTER_DATA = FILTER_DATA & 0b00001111
        bus.write_byte_data(self.ADDRESS, 0x20, FILTER_DATA | (self.FILTER_SELECTED << 4))
      if debug:
        # Checks the filter set
        FILTER_DATA = (bus.read_word_data(self.ADDRESS, 0x20) & 0b11110000) >> 4
        print "(**) Filter is set to:", FILTER_NAME[str(FILTER_DATA)]

      ##############################
      # Block EEPROM to avoid mistakes
      bus.write_byte_data(self.ADDRESS, 0x0D, EE_W & 0b11101111)

  def __extract_data(self, msb, lsb):
    """
    private function to extract data from msb and lsb numbers read from sensor
    details at datasheet, page 52

    """
    acc = ((msb << 8) | lsb) >> 2
    sgn = 1 - ((msb & 0x80) >> 6)
    if (sgn > 0):
      return (acc & 0x1FFF)
    else:
      return ((0b1111111111111 - (acc & 0x1FFF)) * sgn)

  def read(self, return_raw_data=False, debug=False):
    """
    acc = read (return_raw_data=False, debug=False)
    
    It reads x,y,z accelerations

    Parameters
    ----------
    return_raw_data : return raw data or acceleration data? (default: False)
    debug : debug flag (default: False)

    Returns
    -------
    acc : array([acc_x, acc_y, acc_z])

    """

    # Read data from sensor
    data = self.bus.read_i2c_block_data(self.ADDRESS, 0x00, 9)

    # Extract acceleration from raw data
    acc_x = self.__extract_data(data[3], data[2])
    acc_y = self.__extract_data(data[5], data[4])
    acc_z = self.__extract_data(data[7], data[6])

    if debug:
      print acc_x, acc_y, acc_z

    if return_raw_data:
      return array([acc_x, acc_y, acc_z])
    else:
      return array([acc_x, acc_y, acc_z]) * 9.81 / self.SENSIBILITY

class ADXL345:
  """
  Class: ADXL345
  File: accelerometer.py
  Purpose: Python library to work with ADXL345, from Analog Devices
  Created on: 10/07/2016
  """

  def __init__(self, bus, MODE="DEFAULT", RANGE="RANGE_2G", BW=100, debug=False):
    """
    BMA180 constructor

    __init__(bus, OPERATION_MODE, RANGE, FILTER, debug=False)

    Parameters
    ----------
    bus : I2C bus object (where the sensor is connected to)
    
    OPERATION_MODE : string specifying the desired operation mode (default: "LOW_NOISE")
    Possible choices: LOW_NOISE and LOW_POWER

    RANGE : string specifying the range (default: "RANGE_2G")
    Possible choices: RANGE_1G, RANGE_1G5, RANGE_2G, RANGE_3G, RANGE_4G,
                      RANGE_8G and RANGE_16G

    FILTER : string specifying which filter configuration must be used (default: "LOW_300Hz")
    Possible choices: LOW_10Hz, LOW_20Hz, LOW_40Hz, LOW_75Hz, LOW_150Hz, 
                      LOW_300Hz, LOW_600Hz, LOW_1200Hz, HIGH_PASS_1Hz
    
    debug : Show debug information?
    Possible choices: True or False
  
    """

    # Keep bus information as internal variable
    self.bus = bus

    # Some useful ADXL345 definitions
    self.ADDRESS = 0x53
    self._IDENTITY = 0xE5

    # Sensiblity
    self._SENSIBILITY = {"RANGE_2G"  : 1/256. ,
                         "RANGE_4G"  : 1/128. ,
                         "RANGE_8G"  : 1/64. ,
                         "RANGE_16G" : 1/32.  }

    # Possible configuration to range
    self._RANGE_CODE = {"RANGE_2G"  : 0b00 ,
                        "RANGE_4G"  : 0b01 ,
                        "RANGE_8G"  : 0b10 ,
                        "RANGE_16G" : 0b11 }

    # Possible configuration to bandwith
    self._BW_CODE = {0.1  : 0b0000 ,
                     0.2  : 0b0001 ,
                     0.39 : 0b0010 ,
                     0.78 : 0b0011 ,
                     1.56 : 0b0100 ,
                     3.13 : 0b0101 ,
                     6.25 : 0b0110 ,
                     12.5 : 0b0111 ,
                     25   : 0b1000 ,
                     50   : 0b1001 ,
                     100  : 0b1010 ,
                     200  : 0b1011 ,
                     400  : 0b1100 ,
                     800  : 0b1101 ,
                     1600 : 0b1110 ,
                     3200 : 0b1111 }

    # Config mode definition
    self._POWER_MODE = {"DEFAULT"    : 0b00 ,
                        "LOW_POWER"  : 0b01 ,
                        "AUTO_SLEEP" : 0b10 ,
                        "STANDBY"    : 0b11 }

    # Translate the selected configuration
    self._MODE_SELECTED        = self._POWER_MODE[MODE]
    self._SENSIBILITY_SELECTED = self._SENSIBILITY[RANGE]

    # Check if ADXL is working properly
    self.CHIP_ID = bus.read_word_data(self.ADDRESS, 0x00)
    if (self.CHIP_ID != self._IDENTITY):
      print "**************************************************************"
      print "** Problem with accelerometer! Please, abort the operation! **"
      print "** CHIP ID read: ", bin(self.CHIP_ID), "                    **"
      print "**************************************************************"
    else: # If it's working fine, then start setting the ADXL345
      if debug:
        print "(OK) I found the accelerometer ADXL345"

      ##################################
      # Operation mode configuration
      self.set_operation_mode(self._MODE_SELECTED)

      ########################################################
      # Set accelerometer to work with desired gravity range
      self.set_range(RANGE)
      
      # Checks the range set
      if debug:
        inv_range_dict = {v: k for k, v in self._RANGE_CODE.items()}
        RANGE_DATA = (bus.read_word_data(self.ADDRESS, 0x31) & 0b00000011)
        print "(**) Range is set to:", inv_range_dict[RANGE_DATA]

      #####################################
      # Put device in full resolution mode
      self.set_full_resolution_mode()
      
      # Checks the range set
      if debug:
        DATA = (bus.read_word_data(self.ADDRESS, 0x31) & 0b00001000)
        if DATA >> 3:
          print "(**) Device in full resolution mode"
        else:
          print "(**) Device in LOW resolution mode"

      ####################################################
      # Set accelerometer to work with informed bandwidth
      self.set_bw(BW)
      
      # Checks the filter set
      if debug:
        inv_bw_dict = {v: k for k, v in self._BW_CODE.items()}
        DATA = bus.read_word_data(self.ADDRESS, 0x2C) & 0b00001111
        print "(**) Filter is set to:", inv_bw_dict[DATA]

      ###########################
      # Enable the accelerometer
      self.enable_accelerometer()
      
      # Check if the accelerometer is enabled
      if debug:
        DATA = self.bus.read_word_data(self.ADDRESS, 0x2D) & 0b00001000
        if DATA >> 3:
          print "(**) The accelerometer is enabled"
        else:
          print "(**) The accelerometer is disabled"

  def set_operation_mode(self, mode):
    DATA_0x2C = self.bus.read_word_data(self.ADDRESS, 0x2C)
    DATA_0x2D = self.bus.read_word_data(self.ADDRESS, 0x2D)
    if mode == "DEFAULT":
      DATA = DATA_0x2C & 0b11101111
      self.bus.write_byte_data(self.ADDRESS, 0x2C, DATA)
      DATA = DATA_0x2D | 0b00001000
      self.bus.write_byte_data(self.ADDRESS, 0x2D, DATA)
      self.enable_accelerometer()
      self.POWER_MODE = "DEFAULT"
    
    if mode == "LOW_POWER":
      DATA = DATA_0x2C | 0b00010000
      self.bus.write_byte_data(self.ADDRESS, 0x2C, DATA)
      self.POWER_MODE = "LOW_POWER"
    
    if mode == "AUTO_SLEEP":
      print "TO BE ADDED!"
      # set(THRESH_INACT, register 0x25)
      # set(TIME_INACT, register 0x26)
      # set(AUTO_SLEEP D4 and LINK D5, register 0x2D)
      #self.POWER_MODE = "AUTO_SLEEP"
      
    if mode == "STANDBY":
      DATA = (DATA_0x2D & ~0b00001000) & 0b11111111
      self.bus.write_byte_data(self.ADDRESS, 0x2D, DATA)
      self.POWER_MODE = "STANDBY"
    
  def set_range(self, RANGE):
    DATA = self.bus.read_word_data(self.ADDRESS, 0x31)
    DATA_0x31 = (DATA & 0b11111100) | self._RANGE_CODE[RANGE]
    self.bus.write_byte_data(self.ADDRESS, 0x31, DATA_0x31)
  
  def set_full_resolution_mode(self):
    DATA = self.bus.read_word_data(self.ADDRESS, 0x31)
    DATA_0x31 = DATA | 0b00001000
    self.bus.write_byte_data(self.ADDRESS, 0x31, DATA_0x31)
    self.full_resolution = 1
  
  def set_bw(self, BW):
    # TODO: add support to inform user if selected wrong bw in low power mode
    DATA = self.bus.read_word_data(self.ADDRESS, 0x2C)
    DATA_0x2C = (DATA & 0b11110000) | self._BW_CODE[BW]
    self.bus.write_byte_data(self.ADDRESS, 0x2C, DATA_0x2C)

  def enable_accelerometer(self):
    DATA = self.bus.read_word_data(self.ADDRESS, 0x2D)
    DATA_0x2D = DATA | 0b00001000
    self.bus.write_byte_data(self.ADDRESS, 0x2D, DATA_0x2D)

  def disable_accelerometer(self):
    DATA = self.bus.read_word_data(self.ADDRESS, 0x2D)
    DATA_0x2D = DATA & 0b11110111
    self.bus.write_byte_data(self.ADDRESS, 0x2D, DATA_0x2D)
    
  def read(self, return_raw_data=False, debug=False):
    """
    acc = read (return_raw_data=False, debug=False)
    
    It reads x,y,z accelerations

    Parameters
    ----------
    return_raw_data : return raw data or acceleration data? (default: False)
    debug : debug flag (default: False)

    Returns
    -------
    acc : array([acc_x, acc_y, acc_z])

    """

    # Read data from sensor
    data = self.bus.read_i2c_block_data(self.ADDRESS, 0x32, 6)

    # Extract acceleration from raw data
    acc_x = data[0] | (data[1] << 8)
    if (acc_x & (1 << 16 -1)):
      acc_x = acc_x - (1 << 16)

    acc_y = data[2] | (data[3] << 8)
    if (acc_y & (1 << 16 -1)):
      acc_y = acc_y - (1 << 16)

    acc_z = data[4] | (data[5] << 8)
    if (acc_z & (1 << 16 -1)):
      acc_z = acc_z - (1 << 16)

    if debug:
      print acc_x, acc_y, acc_z

    if return_raw_data:
      return array([acc_x, acc_y, acc_z])
    else:
      return array([acc_x, acc_y, acc_z]) * 9.81 * self._SENSIBILITY_SELECTED

def main():
  import smbus
  bus = smbus.SMBus(1)
  accelerometer = BMA180(bus)
  #accelerometer = ADXL345(bus, debug = True)
  while (1):
    data = accelerometer.read()
    g = np.linalg.norm(data)
    pitch, roll = pitchroll(data, g)
    print 'Pitch angle:', (180./np.pi)*pitch, 'Roll angle: ', (180./np.pi)*roll
    time.sleep(0.1)
  print 'ax, ay, az:' + str(data[0]) + ', ' + str(data[1]) + ', ' + str(data[2])
  print 'Pitch angle:', (180./np.pi)*pitch
  print 'Roll angle: ', (180./np.pi)*roll
  bus.close()
  
  return 0

if __name__ == '__main__':
        main()







