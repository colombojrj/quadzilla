# -*- coding: utf-8 -*-
"""
Script to return linear models.
Dependencies: Numpy and Sympy (>=0.7.6).

Authors:
    Arthur Gagg <luizarthur.gagg[AT]gmail.com>
    José Roberto Colombo Junior <colombojrj[AT]gmail.com>
    Pedro Augusto Queiroz de Assis <pedroaugusto.feis[AT]gmail.com>
"""

import numpy as np
import sympy as sym
from util import c2dm

def LoadVars(g = 10.05, rho = 1.2, mq = 1.2, mm = 0.039,
             lx = 28.5e-3, ly = 28.5e-3, lz = 28.5e-3, 
             dcm = 0.2975, Kt = 3.13e-5, Km = 7.5e-7):
  """
  The purpose of this function is to generate an dictionary with
  all parameters of the quadcopter.
  
  Input parameters:
   - None
  
  Output variables:
   - Dictionary with parameters of the quadcopter
  
  TODO list:
   - add support for changing the values herein declared
  """
  p = dict()         # Parameters of the quadcopter are stored in a dictionary
  
  # Some phisical parameters of the quadcopter
  p['g']   = g   # Acceleration of gravity in SJC (m/s^2)
  p['rho'] = rho # Density of air (m^3.kg^-1)
  p['mq']  = mq  # Total mass of the quadcopter
  p['mm']  = mm  # Mass of a motor (kg)
  p['lx']  = lx  # Motor length along x-axis (m)
  p['ly']  = ly  # Motor length along y-axis (m)
  p['lz']  = lz  # Motor length along z-axis (m)
  p['dcm'] = dcm # Distance from the center of gravity to center of a motor (m)
  
  # Distances of the sensors to the center of gravity of the quadcopter
  p['rx'] =  9.2e-3 # m
  p['ry'] = 12.6e-3 # m
  p['rz'] = 40.5e-3 # m

  # Calculate the moment of inertia
  Ix1 = (1/12)*p['mm']*(p['ly']**2+p['lz']**2)                      # Moment of inertia (x-axis) for motors 1 and 3 (kg.m^2)
  Ix2 = (1/12)*p['mm']*(p['ly']**2+p['lz']**2)+p['mm']*p['dcm']**2  # Moment of inertia (x-axis) for motors 2 and 4 (kg.m^2).
  p['Ix'] = 2*Ix1+2*Ix2                                             # Total moment of inertia along the x-axis (kg.m^2)
  Iy1 = (1/12)*p['mm']*(p['lx']**2+p['lz']**2)+p['mm']*p['dcm']**2; # Moment of inertia (y-axis) for motors 1 and 3 (kg.m^2).
  Iy2 = (1/12)*p['mm']*(p['lx']**2+p['lz']**2);                     # Moment of inertia (y-axis) for motors 2 and 4 (kg.m^2).
  p['Iy'] = 2*Iy1+2*Iy2;                                            # Total moment of inertia along the y-axis (kg.m^2)
  Iz1 = (1/12)*p['mm']*(p['lx']**2+p['ly']**2)+p['mm']*p['dcm']**2; # Moment of inertia (z-axis) for motors 1 and 3 (kg.m^2).
  Iz2 = (1/12)*p['mm']*(p['lx']**2+p['ly']**2)+p['mm']*p['dcm']**2; # Moment of inertia (z-axis) for motors 2 and 4 (kg.m^2).
  p['Iz'] = 2*Iz1+2*Iz2;                                            # Total moment of inertia along the z-axis (kg.m^2)
  p['I'] = np.matrix(np.diag([p['Ix'], p['Iy'], p['Iz']]));         # Inertia matrix
  
  # Propeller constants
  p['Km'] = Km;  # Constant value to calculate the moment (kg.m^2.rad^-1)
  p['Kt'] = Kt; # Constant value to calculate the thrust (kg.m.rad^-1)
  p['Ktm'] = p['Km']/p['Kt']; # Constant that relates thrust and moment of a propeller

  # Motors and ESCs constants
  # Recall that 
  # PWMx = mx_k1*wx^2 + mx_k2*wx + mx_offset, where x is some motor
  p['m1_k'] = 37.1834
  p['m1_offset'] = -2102.5553
  p['m2_k'] = 16.5808
  p['m2_offset'] = -738.7423
  p['m3_k'] = 17.7869
  p['m3_offset'] = -835.7571
  p['m4_k'] = 19.1677
  p['m4_offset'] = -880.5494
  p['tau'] = 1/((6.0763 + 5.2313 + 4.7891 + 4.5531)/4)

  return p

def givemeABCD6states(p, debug = False):
  """
  Function to construct the linear state space matrices of the Quad linear model.

  This will be used in the Kalman filter and in the LQR project. The equations of
  the simulation model must be the non-linear ones.
  ---
  
  Model information
  This model have 6 states (just for hover on the base)
  State vector:         X   = [P, Q, R, phi, theta, psi]'
  Linearization point:  Xeq = [0, 0, 0, 0,   0,     0]'
  Input vector: w = [w1, w2, w3, w4]', w in rad/s
  ---
  
  Function information
  Inputs: 
  - p: parameters dictionary
  Outputs: 
  - State space matrices: A, B, C, D
  
  """    
  
  # Calculate the angular velocity to hovering
  w = sym.symbols('w')
  dW = -(p['Kt']/p['mq'])*(w**2 +w**2 +w**2 +w**2) +p['g']
  w0 = float(sym.solve(dW,w)[1])
  if debug:
    print 'w0:', w0
    
  A = np.matrix([[0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0], 
                 [0, 0, 0, 0, 0, 0],
                 [1, 0, 0, 0, 0, 0],
                 [0, 1, 0, 0, 0, 0],
                 [0, 0, 1, 0, 0, 0]])
              
  fx = 2*p['Kt']*p['dcm']*w0/p['Ix']  
  fy = 2*p['Kt']*p['dcm']*w0/p['Iy']
  fz = 2*p['Km']*p['dcm']*w0/p['Iz']
  B = np.matrix([[  0,  fx,   0, -fx],
                 [ fy,   0, -fy,   0],
                 [ fz, -fz,  fz, -fz],
                 [  0,   0,   0,   0],
                 [  0,   0,   0,   0],
                 [  0,   0,   0,   0]])
  
  C = np.matrix(np.eye(6,6))
  D = np.matrix(np.zeros((6,4)))

  return [A,B,C,D]

def givemeABCD8states(p):
  """
  Function to construct the linear state space matrices of the Quad linear model.

  This will be used in the Kalman filter and in the LQR project. The equations of
  the simulation model must be the non-linear ones.
  ---
  
  Model information
  This model have 8 states (just for hover)
  State vector:         X   = [W, P, Q, R, Z, phi, theta, psi]'
  Linearization point:  Xeq = [0, 0, 0, 0, Z,  0,    0,     0]'
  Input vector: w = [w1, w2, w3, w4]', w in rad/s
  ---
  
  Function information
  Inputs: 
  - p: parameters dictionary
  Outputs: 
  - State space matrices: A, B, C, D
  
  """    
  
  # Calculate the angular velocity to hovering
  w = sym.symbols('w')
  dW = -(p['Kt']/p['mq'])*(w**2 +w**2 +w**2 +w**2) +p['g']
  w0 = float(sym.solve(dW,w)[1])
    
  A = np.matrix([[0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0], 
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0],
                 [1, 0, 0, 0, 0, 0],
                 [0, 1, 0, 0, 0, 0],
                 [0, 0, 1, 0, 0, 0],
                 [0, 0, 0, 1, 0, 0]])
  fs = 2*p['Kt']*w0/p['mq']
  fx = 2*p['Kt']*p['dcm']*w0/p['Ix']  
  fy = 2*p['Kt']*p['dcm']*w0/p['Iy']
  fz = 2*p['Km']*p['dcm']*w0/p['Iz']
  B = np.matrix([[ -fs,-fs, -fs, -fs],
                 [  0,  fx,   0, -fx],
                 [ fy,   0, -fy,   0],
                 [ fz, -fz,  fz, -fz],
                 [  0,   0,   0,   0],
                 [  0,   0,   0,   0],
                 [  0,   0,   0,   0],
                 [  0,   0,   0,   0]])
  
  C = np.matrix(np.zeros((7,8)))
  C[0,1] = 1
  C[1,2] = 1
  C[2,3] = 1
  C[3,5] = 1
  C[4,6] = 1
  C[5,7] = 1
  C[6,4] = 1
  
  D = np.matrix(np.zeros((7,4)))

  return [A,B,C,D]    

def DesignOutputK(A, B, C, Wx, Wu):
  import picos as pic
  from scipy.linalg import sqrtm
  
  # Some useful sizes
  nx = np.shape(A)[0]
  nu = np.shape(B)[1]
  ny = np.shape(C)[0]
  
  # Create and solve the LMI based optimization problem
  prob = pic.Problem()
  Q = prob.add_variable('Q', (nx,nx), 'symmetric')
  Qt= prob.add_variable('Qt', (ny,ny), 'symmetric')
  Y = prob.add_variable('Y', (nu,ny))
  gamma = prob.add_variable('gamma')
  Znx = pic.new_param('Znxnx', np.zeros((nx,nx)))
  Znunx = pic.new_param('Znunx', np.zeros((nu,nx)))
  Enx = pic.new_param('Enx',   np.eye(nx,nx))
  Enu = pic.new_param('Enx',   np.eye(nu,nu))
  Eny = pic.new_param('Eny', np.eye(ny,ny))
  A = pic.new_param('A', A)
  B = pic.new_param('B', B)
  C = pic.new_param('C', C)
  Wx = pic.new_param('Wx', sqrtm(Wx))
  Wu = pic.new_param('Wu', sqrtm(Wu))
  prob.add_constraint( Q >> np.zeros((nx,nx)) )
  prob.add_constraint( gamma > 0 )
  
  # Add the stability constraint
  ZERO = np.zeros((3*nx+nu,3*nx+nu))
  a21 = A*Q+B*Y*C
  a31 = Wx*Q
  a41 = Wu*Y*C
  prob.add_constraint( 
    (Q     & a21.T  & a31.T      & a41.T)        // 
    (a21   & Q      & Znx        & Znunx.T)      // 
    (a31   & Znx    & gamma*Enx  & Znunx.T)      // 
    (a41   & Znunx  & Znunx      & Enu*gamma)    >> ZERO )
  
  # Add the equality constraint
  ZERO = np.zeros((nx+ny,nx+ny))
  a21 = C*Q -Qt*C
  prob.add_constraint( 
    (1e-12*Enx  & a21.T  )      // 
    (a21        & Eny    )      >> ZERO )
    
  # Solve the optimization problem
  prob.set_objective('min', gamma)
  sol = prob.solve(solver='cvxopt', verbose=False, feastol=1e-6, solve_via_dual=False)
  
  # Calculate the gain matrix and return it
  Qt = np.matrix(Qt.value)
  Y = np.matrix(Y.value)
  F = Y*np.linalg.inv(Qt)
  return F

def DesignLQRintegrator(Ad, Bd, Cd, Wx, Wu):
  from util import dlqr
  l1 = np.hstack((Ad, Bd))
  l2 = np.hstack((np.zeros((4,6)), np.eye(4,4)))
  At = np.vstack((l1, l2))
  Bt = np.vstack((np.zeros((6,4)), np.eye(4,4)))
  Ct = np.hstack((Cd, np.zeros((6,4))))
  #K, P = dlqr(At, Bt, np.eye(10,10), np.eye(4,4))
  
  #Wx = np.matrix(np.diag((1e-3,1e-3,1e-3,1e3,1e3,1e3,1e-6,1e-6,1e-6,1e-6)))
  #Wu = 1e-6*np.matrix(np.eye(4,4))
  F = DesignOutputK(At, Bt, Ct, Wx, Wu)
  
  return F

# Test function
if __name__ == '__main__':
  from util import *
  from control import place
  import numpy as np
  parameters = LoadVars()
  [A, B, C, D] = givemeABCD6states(parameters, debug = True)
  [Ad, Bd] = c2dm({0:A}, {0:B}, 0.05)
  Ad = Ad[0]
  Bd = Bd[0]
  print 'The matrices A and B were discretized successfully!'
  
  print '##################'
  print '# Pole placement #'
  print '##################'
  poles = [0.7] * 6  
  K = np.matrix(place(Ad,Bd,poles))
  D, V = np.linalg.eig(Ad-Bd*K)
  print 'The poles were placed to:'
  print np.real(D)
  
  
