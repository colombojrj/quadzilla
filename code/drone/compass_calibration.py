import compass
import gyroscope
import smbus, time
from scipy.io import savemat
from numpy import zeros

# Start I2C communication
bus = smbus.SMBus(1)

# Initialize the sensors
mag = compass.HCM5883(bus)
gyro= gyroscope.ITG_3200(bus)

# Delay and execution time
tend = 20
delay = 10e-3

# Initialize the data array
mag_data = zeros((3,int(tend/delay)))
gyro_data= zeros((3,int(tend/delay)))

for k in xrange(0,int(tend/delay)):
  gyro_aux = gyro.read()
  mag_aux  = mag.read()
  print k
  for i in xrange(0,3):
    gyro_data[i,k]= gyro_aux[i]
    mag_data[i,k] = mag_aux[i]
  
  time.sleep(delay)

savemat('compass_calibration_data.mat', {'mag_data':mag_data,'gyro_data':gyro_data})
bus.close()

